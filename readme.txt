#
# About
#

A Game Boy emulator, created as a learning experience. Emulates the first
Nintendo Game Boy.

Supports everything except sounds and link cables at the moment.

#
# Requires
#

A C++20 compiler. The build system is CMake with a bundled SFML library
for window display and keyboard functionality.
