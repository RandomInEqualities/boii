#include "gb_exception.hpp"

#include <filesystem>
#include <format>

namespace gb
{

Exception::Exception(const std::string& message)
    : std::runtime_error(message)
{
}

AssertException::AssertException(const std::string& condition, const std::source_location& location)
    : Exception(std::format("Assert failed, {0}[{1}]: {2}", location.file_name(), location.line(), condition))
{
}

InternalErrorException::InternalErrorException()
    : Exception("internal error")
{
}

} // namespace gb
