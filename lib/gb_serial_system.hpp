#pragma once

#include "gb_int.hpp"

#include <functional>

namespace gb
{

/**
 * Serial communication system to enable and emulate multiplayer games.
 */
class SerialSystem
{
public:
    /**
     * Write the given byte at the specified address.
     */
    void write(Address, Byte);

    /**
     * Read the byte at the specified address.
     */
    Byte read(Address) const;

    /**
     * Check if the serial system has an interrupt.
     */
    bool interrupt(Cycles cycles);

    /**
     * Set a function that is called on a serial port write.
     */
    using TransferNotifier = std::function<void(Byte)>;
    void setTransferNotifier(const TransferNotifier&);

private:
    bool m_useInternalClock = false;
    bool m_hasTransfer = false;
    Byte m_transferData = 0;
    TransferNotifier m_transferNotifier;
};

} // namespace gb
