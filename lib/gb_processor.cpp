#include "gb_processor.hpp"
#include "gb_instructions.hpp"
#include "gb_string_utilities.hpp"

namespace gb
{

static void logRegisters(Logger& log, const Registers& registers)
{
    if (log)
    {
        const auto format8 = [&](const Register id)
        {
            return std::string(toString(id)) + ": " + toHex(registers.get(id));
        };
        const auto format16 = [&](const RegisterPair id)
        {
            return std::string(toString(id)) + ": " + toHex(registers.get(id));
        };
        log() << "=======================";
        log() << format8(Register::A) << format8(Register::F);
        log() << format8(Register::B) << format8(Register::C);
        log() << format8(Register::D) << format8(Register::E);
        log() << format8(Register::H) << format8(Register::L);
        log() << format16(RegisterPair::PC);
        log() << format16(RegisterPair::SP);
    }
}

static void logCartridge(Logger& log, const Cartridge& cartridge)
{
    if (log)
    {
        cartridge.dump(log);
    }
}

static void logInterrupt(Logger& log, const Address interruptAddress)
{
    if (log)
    {
        log() << "interrupt" << toHex(interruptAddress);
    }
}

static void logInstruction(Logger& log, const uint8 instruction)
{
    if (log)
    {
        log() << "instruction" << toHex(instruction) << toBinary(instruction);
    }
}

Processor::Processor(Cartridge& cartridge)
    : m_cartridge(cartridge), m_memory(m_registers, m_systems, cartridge)
{
}

InputSystem& Processor::input()
{
    return m_systems.input;
}

LcdSystem& Processor::lcd()
{
    return m_systems.lcd;
}

SoundSystem& Processor::sound()
{
    return m_systems.sound;
}

SerialSystem& Processor::serial()
{
    return m_systems.serial;
}

Cycles Processor::step(Logger& log)
{
    /**
     * Execute the next instruction. Executes on an atomic macro level, not simulating
     * hardware clock cycles in detail. This is not 100% correct, but it seems to work.
     */
    const Cycles machineCycles = executeNextInstruction(log);
    GB_ASSERT(machineCycles > 0);

    /**
     * Propagate the cycles to other systems, so they can update, for example timers,
     * screen scanline rendering, serial communication and input key press handling.
     */
    propagateCycles(machineCycles);

    /**
     * Check if any system wants to issue an interrupt during the intruction cycles.
     * See page 26 of Game Boy Programming manual for a good overview of this!
     */
    if (const std::optional<Address> interrupt = checkForInterrupt())
    {
        logInterrupt(log, *interrupt);
        jumpTo(*interrupt);
    }

    return machineCycles;
}

Cycles Processor::executeNextInstruction(Logger& log)
{
    if (m_registers.isProcessorStopped())
    {
        return 1;
    }
    if (m_registers.isProcessorHalted())
    {
        return 1;
    }

    const uint8 nextInstruction = takeInstruction();
    logInstruction(log, nextInstruction);
    const Cycles instructionCycles = execute(nextInstruction, m_registers, m_memory, log);
    const Cycles machineCycles = 4 * instructionCycles;
    logRegisters(log, m_registers);
    logCartridge(log, m_cartridge);
    return machineCycles;
}

uint8 Processor::takeInstruction()
{
    const uint16 pc = m_registers.pc();
    const uint8 opCode = m_memory.read(pc);
    m_registers.setPc(pc + 1);
    return opCode;
}

void Processor::jumpTo(const Address address)
{
    const uint16 pc = m_registers.pc();
    const uint16 sp = m_registers.sp();
    m_memory.write(sp - 1, getHigh(pc));
    m_memory.write(sp - 2, getLow(pc));
    m_registers.setSp(sp - 2);
    m_registers.setPc(address);
}

void Processor::propagateCycles(const Cycles cycles)
{
    if (m_systems.lcd.interrupt(cycles))
    {
        if (m_systems.lcd.haveVBlankInterrupt())
        {
            m_registers.setInterruptAtIndex(0);
        }
        if (m_systems.lcd.haveStatInterrupt())
        {
            m_registers.setInterruptAtIndex(1);
        }
    }
    if (m_systems.timer.interrupt(cycles))
    {
        m_registers.setInterruptAtIndex(2);
    }
    if (m_systems.serial.interrupt(cycles))
    {
        m_registers.setInterruptAtIndex(3);
    }
    if (m_systems.input.interrupt(cycles))
    {
        m_registers.setInterruptAtIndex(4);

        // The processor wakes up when an input key is triggered.
        m_registers.setProcessorStopped(false);
    }
}

std::optional<Address> Processor::checkForInterrupt()
{
    if (m_registers.haveAnyInterrupt())
    {
        if (m_registers.isInterruptsEnabled())
        {
            return processNormalInterrupt();
        }
        if (m_registers.isProcessorHalted())
        {
            return processHaltInterrupt();
        }
    }
    return std::nullopt;
}

Address Processor::processNormalInterrupt()
{
    m_registers.setInterruptsEnabled(false);
    m_registers.clearProcessorHalt();

    if (m_registers.haveInterruptAtIndex(0))
    {
        m_registers.resetInterruptAtIndex(0);
        return 0x0040;
    }
    if (m_registers.haveInterruptAtIndex(1))
    {
        m_registers.resetInterruptAtIndex(1);
        return 0x0048;
    }
    if (m_registers.haveInterruptAtIndex(2))
    {
        m_registers.resetInterruptAtIndex(2);
        return 0x0050;
    }
    if (m_registers.haveInterruptAtIndex(3))
    {
        m_registers.resetInterruptAtIndex(3);
        return 0x0058;
    }
    if (m_registers.haveInterruptAtIndex(4))
    {
        m_registers.resetInterruptAtIndex(4);
        return 0x0060;
    }
    throw InternalErrorException();
}

Address Processor::processHaltInterrupt()
{
    const Address jumpAddress = m_registers.getProcessorHalt();
    m_registers.clearProcessorHalt();
    return jumpAddress;
}

} // namespace gb
