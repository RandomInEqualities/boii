#pragma once

#include "gb_cycle_counter.hpp"
#include "gb_int.hpp"

namespace gb
{

/**
 * Timer system to generate periodic interrupts for games.
 *
 * It consists of the TIMA, TAC and TMA ports that controls the period of
 * the interrupts. Se page 25 in Game Boy Programming Manual.
 */
class TimerSystem
{
public:
    explicit TimerSystem();

    /**
     * Write the given byte at the specified address.
     */
    void write(Address, Byte);

    /**
     * Read the byte at the specified address.
     */
    Byte read(Address) const;

    /**
     * Check if any timer should trigger an interrupt.
     */
    bool interrupt(Cycles cycles);

private:
    bool stepDiv(Cycles cycles);
    bool stepTima(Cycles cycles);

    /**
     * DIV: register that increments at 16384Hz and resets to 0x0 on overflow.
     */
    CycleCounter m_divCycleCounter;
    int32 m_divCounter = 0;

    /**
     * TIMA, TMA, TAC: configurable timer. The current amount of clock counts
     * is in TIMA, resets to TMA on overflow (>255), frequency of TIMA increments
     * controlled by TAC.
     */
    CycleCounter m_timaCycleCounter;
    int32 m_timaCounter = 0;
    int32 m_timaReset = 0;
    Byte m_tac = 0;
};

} // namespace gb
