#pragma once

#include "gb_byte.hpp"
#include "gb_int.hpp"

#include <vector>

namespace gb
{

/**
 * An RGB color that the LCD screen can draw.
 */
struct LcdColor
{
    uint8 red = 255;
    uint8 green = 255;
    uint8 blue = 255;
};

/**
 * An image that the LCD screen can generate.
 */
class LcdImage
{
public:
    using Pixels = std::vector<LcdColor>;
    explicit LcdImage(int32 width, int32 height, Pixels);

    /**
     * Get the width or height of the image.
     */
    int32 width() const;
    int32 height() const;

    /**
     * Get the pixels in the image.
     */
    const Pixels& pixels() const;

    /**
     * Convert the image to an RGBA byte array.
     */
    std::vector<uint8> toRgba() const;

private:
    int32 m_width = 0;
    int32 m_height = 0;
    Pixels m_pixels;
};

/**
 * System for the Game Boy LCD screen to show the Game Boy graphics.
 *
 * Use getScreenImage to get the current state of the lcd screen (which is
 * what should be shown).
 *
 * The Game Boy hardware runs this screen at 60Hz, with 60 generated frames
 * per second.
 */
class LcdSystem
{
public:
    explicit LcdSystem();

    /**
     * What is displayed currently on the LCD screen.
     */
    LcdImage getScreenImage() const;

    /**
     * Screen dump of the video memory. Shows the tiles in video memory.
     */
    LcdImage getTileImage() const;

    /**
     * Write the given byte at the specified address.
     */
    void write(Address, Byte);

    /**
     * Read the byte at the specified address.
     */
    Byte read(Address) const;

    /**
     * Check if the LCD screen has an interrupt.
     */
    bool interrupt(Cycles cycles);

    /**
     * Returns true if the last call to interrupt generated a v-blank interrupt.
     *
     * True at the point where a new frame has been fully rendered.
     */
    bool haveVBlankInterrupt() const;

    /**
     * Returns true if the last call to interrupt generated a stat interrupt.
     *
     * True at the point where a game defined interrupt happens on a scanline or h-blank.
     */
    bool haveStatInterrupt() const;

private:
    /**
     * We need to go through 4 GPU modes each frame. A single frame takes in total 70224 clocks.
     */
    enum class Mode : uint8
    {
        /**
         * Mode 00: CPU can access both video RAM (8000h-9FFFh) and OAM (FE00h-FE9Fh).
         * Takes 201-207 clocks. Occurs 144 times per frame.
         */
        HBlank,

        /**
         * Mode 01: CPU can access both video RAM and OAM.
         * Takes 4560 clocks at the end of the frame. Occurs once per frame.
         */
        VBlank,

        /**
         * Mode 10: LCD system is reading from OAM memory. CPU can access display VRAM.
         * Takes 77-83 clocks. Occurs 144 times per frame.
         */
        InOAM,

        /**
         * Mode 11: The LCD system is reading from both OAM and VRAM.
         * Takes 169-175 clocks. Occurs 144 times per frame.
         */
        InOAMAndVideoRAM
    };

    /**
     * Description of a sprite to display on the screen.
     */
    struct Sprite
    {
        int32 xPosition = 0;
        int32 yPosition = 0;
        uint8 code = 0;
        uint8 attribute = 0;
    };

    Byte getStatusRegister() const;
    bool haveLycInterrupt() const;

    /**
     * Draw a whole y-pixel scanline into the backing store image.
     */
    void drawScanline(int32 yPosition);
    void drawBackground(int32 yPosition, Address codeArea, Address characterArea);
    void drawWindow(int32 yPosition, Address codeArea, Address characterArea);
    void drawSprites(int32 yPosition, bool isDrawMode8x16);

    /**
     * Get the two color bits in the (x,y) pixel in the tile at address. (x,y) should be between [0,7].
     */
    uint8 getColorBitsInDot8x8(Address address, uint8 x, uint8 y) const;

    /**
     * Get the rgb color in the (x,y) pixel in the tile at address. (x,y) should be between [0,7].
     */
    LcdColor getBackgroundColorDot8x8(Address address, uint8 x, uint8 y) const;

    /**
     * Get the address for the tile at the xDot and yDot position in the background map.
     */
    Address getBackgroundTileAddress(Address codeArea, Address characterArea, int32 xDot, int32 yDot) const;

    /**
     * Get the sprites that should be drawn at the given y-pixel line - in sorted order.
     */
    std::vector<Sprite> getSpritesInScanline(int32 yPosition, bool isDrawMode8x16) const;

    /**
     * Adjust the sprite in 8x16 draw mode; we might need to draw the upper part of the 8x16 sprite.
     */
    static Sprite adjustSpriteFor8x16Mode(Sprite sprite, int32 yPosition);

    /**
     * Video RAM for holding tile data and background map.
     */
    ByteVector m_videoRam;

    /**
     * Sprite OAM for holding indexes to the sprites to draw.
     */
    ByteVector m_oam;

    /**
     * 0xFF40: control register for drawing.
     */
    Byte m_drawControlRegister = 0x91;

    /**
     * 0xFF41: stat register for interrupts.
     */
    Mode m_mode = Mode::VBlank;
    Cycles m_modeClocksLeft = 0;
    Cycles m_modeVblanksClocksLeft = 0;
    bool m_interruptOnHBlank = false;
    bool m_interruptOnVBlank = false;
    bool m_interruptOnInOam = false;
    bool m_interruptOnLycMatch = false;

    /**
     * Set when specific interrupts occur.
     */
    bool m_haveVBlankInterrupt = false;
    bool m_haveStatInterrupt = false;

    /**
     * 0xFF42, 0xFF43: x and y scroll position in the background map.
     */
    int32 m_backgroundScrollX = 0;
    int32 m_backgroundScrollY = 0;

    /**
     * 0xFF44: ly register, current line being processed in LCD.
     * 0xFF45: lyc register, match flag on stat register set when ly matches lyc.
     */
    int32 m_ly = 0;
    int32 m_lyc = 0;

    /**
     * 0xFF47, 0xFF48, 0xFF49: palette selection for background and sprites.
     */
    Byte m_backgroundPalette = 0xFC;
    Byte m_spritePalette0 = 0xFF;
    Byte m_spritePalette1 = 0xFF;

    /**
     * 0xFF4A, 0xFF4B: x and y positions of alternate 'Window' background.
     * If at 0,0 it completely covers existing background.
     */
    int32 m_windowBackgroundX = 0;
    int32 m_windowBackgroundY = 0;

    /**
     * The image that simulates the LCD screen.
     */
    const int32 m_width = 160;
    const int32 m_height = 144;
    LcdImage::Pixels m_pixels;
};

} // namespace gb
