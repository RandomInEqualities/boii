#include "gb_memory.hpp"
#include "gb_bit_utilities.hpp"
#include "gb_exception.hpp"

namespace gb
{

Memory::Memory(Registers& registers, Systems& systems, Cartridge& cartridge)
    : m_registers(registers), m_systems(systems), m_cartridge(cartridge)
{
    m_lowRam = createRandomBytes(0x2000);
    m_highRam = createRandomBytes(0xFFFF - 0xFF80);
}

void Memory::write(const Address address, const Byte value)
{
    if (address < 0x4000)
    {
        // ROM bank 0.
        m_cartridge.write(address, value);
    }
    else if (address >= 0x4000 && address < 0x8000)
    {
        // Switchable ROM bank.
        m_cartridge.write(address, value);
    }
    else if (address >= 0x8000 && address < 0xA000)
    {
        // Video RAM.
        m_systems.lcd.write(address, value);
    }
    else if (address >= 0xA000 && address < 0xC000)
    {
        // Switchable RAM bank.
        m_cartridge.write(address, value);
    }
    else if (address >= 0xC000 && address < 0xE000)
    {
        // RAM.
        m_lowRam.at(address - 0xC000) = value;
    }
    else if (address >= 0xE000 && address < 0xFE00)
    {
        // Echo of RAM 0xC000 to 0xDE00.
        write(0xC000 + (address - 0xE000), value);
    }
    else if (address >= 0xFE00 && address < 0xFEA0)
    {
        // Sprite attribute space.
        m_systems.lcd.write(address, value);
    }
    else if (address >= 0xFEA0 && address < 0xFF00)
    {
        // Unusable and Empty.
        // Some games may write them anyway (Tetris World).
    }
    else if (address == 0xFF00)
    {
        // Input control.
        m_systems.input.write(address, value);
    }
    else if (address == 0xFF01 || address == 0xFF02)
    {
        // Serial communication.
        m_systems.serial.write(address, value);
    }
    else if (address >= 0xFF04 && address <= 0xFF07)
    {
        // Timer control.
        m_systems.timer.write(address, value);
    }
    else if (address == 0xFF0F)
    {
        // Interrupt control.
        m_registers.setSignalledInterrupts(value);
    }
    else if (address >= 0xFF10 && address < 0xFF40)
    {
        // Sound control.
        m_systems.sound.write(address, value);
    }
    else if (address >= 0xFF40 && address < 0xFF46)
    {
        // Screen control.
        m_systems.lcd.write(address, value);
    }
    else if (address == 0xFF46)
    {
        // DMA transfer from RAM to OAM.
        dmaTransfer(value);
    }
    else if (address >= 0xFF47 && address < 0xFF4C)
    {
        // Screen control.
        m_systems.lcd.write(address, value);
    }
    else if (address >= 0xFF4C && address < 0xFF80)
    {
        // Unusable and empty. Some of them are used in GBC.
        // Some games may write them anyway (Pokémon Yellow).
    }
    else if (address >= 0xFF80 && address < 0xFFFF)
    {
        // RAM.
        m_highRam.at(address - 0xFF80) = value;
    }
    else if (address == 0xFFFF)
    {
        // Interrupt control.
        m_registers.setEnabledInterrupts(value);
    }
    else
    {
        throw InternalErrorException();
    }
}

Byte Memory::read(const Address address) const
{
    if (address < 0x4000)
    {
        // ROM bank 0.
        return m_cartridge.read(address);
    }
    else if (address >= 0x4000 && address < 0x8000)
    {
        // Switchable ROM bank.
        return m_cartridge.read(address);
    }
    else if (address >= 0x8000 && address < 0xA000)
    {
        // Video RAM.
        return m_systems.lcd.read(address);
    }
    else if (address >= 0xA000 && address < 0xC000)
    {
        // Switchable RAM bank.
        return m_cartridge.read(address);
    }
    else if (address >= 0xC000 && address < 0xE000)
    {
        // RAM.
        return m_lowRam.at(address - 0xC000);
    }
    else if (address >= 0xE000 && address < 0xFE00)
    {
        // Echo of RAM 0xC000 to 0xDE00.
        return read(0xC000 + (address - 0xE000));
    }
    else if (address >= 0xFE00 && address < 0xFEA0)
    {
        // Sprite attribute space.
        return m_systems.lcd.read(address);
    }
    else if (address >= 0xFEA0 && address < 0xFF00)
    {
        // Unusable and Empty.
        // Some games may read them anyway (Tetris World).
        return 0xFF;
    }
    else if (address == 0xFF00)
    {
        // Input control.
        return m_systems.input.read(address);
    }
    else if (address == 0xFF01 || address == 0xFF02)
    {
        // Serial communication.
        return m_systems.serial.read(address);
    }
    else if (address >= 0xFF04 && address <= 0xFF07)
    {
        // Timer control.
        return m_systems.timer.read(address);
    }
    else if (address == 0xFF0F)
    {
        // Interrupt control.
        return m_registers.getSignalledInterrupts();
    }
    else if (address >= 0xFF10 && address < 0xFF40)
    {
        // Sound control.
        return m_systems.sound.read(address);
    }
    else if (address >= 0xFF40 && address < 0xFF46)
    {
        // Screen control.
        return m_systems.lcd.read(address);
    }
    else if (address == 0xFF46)
    {
        throw UnimplementedException("read from dma transfer register");
    }
    else if (address >= 0xFF47 && address < 0xFF4C)
    {
        // Screen control.
        return m_systems.lcd.read(address);
    }
    else if (address >= 0xFF4C && address < 0xFF80)
    {
        // Unusable and Empty. Some of them are used in GBC.
        // Some games may read them anyway (Pokémon Yellow).
        return 0xFF;
    }
    else if (address >= 0xFF80 && address < 0xFFFF)
    {
        // RAM.
        return m_highRam.at(address - 0xFF80);
    }
    else if (address == 0xFFFF)
    {
        // Interrupt control.
        return m_registers.getEnabledInterrupts();
    }
    else
    {
        throw InternalErrorException();
    }
}

void Memory::dmaTransfer(const uint8 request)
{
    uint16 startAddress = 0;
    setHigh(startAddress, request);

    if (startAddress >= 0x8000 && startAddress < 0xE000)
    {
        for (uint16 offset = 0; offset < 0xA0; offset++)
        {
            const uint8 value = read(startAddress + offset);
            write(0xFE00 + offset, value);
        }
    }
    else
    {
        throw UnsupportedException("dma transfer from bad address");
    }
}

} // namespace gb
