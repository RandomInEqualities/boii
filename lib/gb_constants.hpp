#pragma once

#include "gb_int.hpp"

namespace gb::constant
{

/**
 * The Game Boy CPU runs at 4194304 clocks per second (Hz).
 */
constexpr Cycles cpuCyclesPerSecond = 4194304;

} // namespace gb::constant
