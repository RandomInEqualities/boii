#pragma once

#include "gb_byte.hpp"
#include "gb_int.hpp"
#include "gb_logger.hpp"

#include <memory>
#include <string>

namespace gb
{

/**
 * A memory bank controller for a Game Boy cartridge.
 */
class MemoryBankController
{
public:
    virtual ~MemoryBankController() = default;

    /**
     * Get the cartridges title.
     */
    virtual std::string title() const = 0;

    /**
     * Get the bytes in the cartridges current ram (for save game support).
     */
    virtual const ByteVector& ram() const = 0;

    /**
     * Set the bytes in the cartridges current ram (for save game support).
     */
    virtual void setRam(const ByteVector& ram) = 0;

    /**
     * Dump a summary of the cartridge to the given debug logger.
     */
    virtual void dump(Logger& log) const = 0;

    /**
     * Write a byte into the cartridge at the specified address.
     */
    virtual void write(Address address, Byte value) = 0;

    /**
     * Read a byte from the cartridge at the specified address.
     */
    virtual Byte read(Address address) const = 0;
};

/**
 * A Game Boy cartridge.
 *
 * There are many different cartridges (MBC1, MBC2, MBC3, MBC5, MBC6). Each type also have different
 * variants with different amount of ROM/RAM/extra functionality/batteries. This class tries to
 * abstract all those away behind a simple interface.
 *
 * Summary of different types of cartridges: http://gbdev.gg8.se/wiki/articles/The_Cartridge_Header
 */
class Cartridge
{
public:
    explicit Cartridge(const ByteVector& rom);

    /**
     * Write a byte into the cartridge at the specified address.
     */
    void write(Address, Byte);

    /**
     * Read a byte from the cartridge at the specified address.
     */
    Byte read(Address) const;

    /**
     * Get the cartridges title.
     */
    std::string title() const;

    /**
     * Get the bytes in the cartridges current ram (for save game support).
     */
    ByteVector ram() const;

    /**
     * Set the bytes in the cartridges current ram (for save game support).
     */
    void setRam(const ByteVector& ram);

    /**
     * Dump a summary of the cartridge to the given debug logger.
     */
    void dump(Logger& log) const;

private:
    std::unique_ptr<MemoryBankController> m_controller;
};

} // namespace gb
