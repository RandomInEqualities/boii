#include "gb_input_system.hpp"
#include "gb_bit_utilities.hpp"
#include "gb_exception.hpp"

namespace gb
{

InputSystem::InputSystem()
{
    m_isPressed[Key::Right] = false;
    m_isPressed[Key::Left] = false;
    m_isPressed[Key::Up] = false;
    m_isPressed[Key::Down] = false;
    m_isPressed[Key::A] = false;
    m_isPressed[Key::B] = false;
    m_isPressed[Key::Select] = false;
    m_isPressed[Key::Start] = false;
}

void InputSystem::setPressed(const Key key, const bool isPressed)
{
    m_isPressed[key] = isPressed;
    if (isPressed)
    {
        m_haveInterrupt = true;
    }
}

void InputSystem::write(const Address address, const Byte value)
{
    if (address == 0xFF00)
    {
        m_useDirectionKeys = !isBitTrue(value, 4);
        m_useSelectionKeys = !isBitTrue(value, 5);
    }
    else
    {
        throw InternalErrorException();
    }
}

Byte InputSystem::read(const Address address) const
{
    if (address == 0xFF00)
    {
        Byte value = 0xFF;

        if (m_useDirectionKeys)
        {
            setBit(value, 0, !m_isPressed.at(Key::Right));
            setBit(value, 1, !m_isPressed.at(Key::Left));
            setBit(value, 2, !m_isPressed.at(Key::Up));
            setBit(value, 3, !m_isPressed.at(Key::Down));
        }
        else if (m_useSelectionKeys)
        {
            setBit(value, 0, !m_isPressed.at(Key::A));
            setBit(value, 1, !m_isPressed.at(Key::B));
            setBit(value, 2, !m_isPressed.at(Key::Select));
            setBit(value, 3, !m_isPressed.at(Key::Start));
        }

        setBit(value, 4, !m_useDirectionKeys);
        setBit(value, 5, !m_useSelectionKeys);

        return value;
    }
    else
    {
        throw InternalErrorException();
    }
}

bool InputSystem::interrupt(const Cycles /*cycles*/)
{
    if (m_haveInterrupt)
    {
        m_haveInterrupt = false;
        return true;
    }
    return false;
}

} // namespace gb
