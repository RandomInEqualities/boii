#pragma once

#include "gb_input_system.hpp"
#include "gb_lcd_system.hpp"
#include "gb_serial_system.hpp"
#include "gb_sound_system.hpp"
#include "gb_timer_system.hpp"

namespace gb
{

/**
 * The emulated hardware systems on the Game Boy.
 */
struct Systems
{
    /**
     * The system that emulates the LCD screen.
     */
    LcdSystem lcd;

    /**
     * The system that emulates timers.
     */
    TimerSystem timer;

    /**
     * The system that emulates button presses.
     */
    InputSystem input;

    /**
     * The system that emulates sounds.
     */
    SoundSystem sound;

    /**
     * The system that emulates serial communication.
     */
    SerialSystem serial;
};

} // namespace gb
