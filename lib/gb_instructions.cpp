#include "gb_instructions.hpp"
#include "gb_bit_utilities.hpp"
#include "gb_exception.hpp"
#include "gb_int_cast.hpp"
#include "gb_string_utilities.hpp"

#include <bit>

namespace gb
{
namespace
{

/**
 * The program state passed to each instruction.
 */
struct ProgramState
{
    /**
     * The instruction that is getting executed.
     */
    uint8 instruction;

    /**
     * The CPU registers.
     */
    Registers& registers;

    /**
     * The CPU memory.
     */
    Memory& memory;

    /**
     * A logger for logging debug text.
     */
    Logger& log;
};

constexpr Register bitsToRegister(const uint8 bits)
{
    if (bits == 0b111)
    {
        return Register::A;
    }
    if (bits == 0b000)
    {
        return Register::B;
    }
    if (bits == 0b001)
    {
        return Register::C;
    }
    if (bits == 0b010)
    {
        return Register::D;
    }
    if (bits == 0b011)
    {
        return Register::E;
    }
    if (bits == 0b100)
    {
        return Register::H;
    }
    if (bits == 0b101)
    {
        return Register::L;
    }
    throw InternalErrorException();
}

constexpr RegisterPair ddToRegister(const uint8 dd)
{
    if (dd == 0b00)
    {
        return RegisterPair::BC;
    }
    if (dd == 0b01)
    {
        return RegisterPair::DE;
    }
    if (dd == 0b10)
    {
        return RegisterPair::HL;
    }
    if (dd == 0b11)
    {
        return RegisterPair::SP;
    }
    throw InternalErrorException();
}

constexpr RegisterPair qqToRegister(const uint8 qq)
{
    if (qq == 0b00)
    {
        return RegisterPair::BC;
    }
    if (qq == 0b01)
    {
        return RegisterPair::DE;
    }
    if (qq == 0b10)
    {
        return RegisterPair::HL;
    }
    if (qq == 0b11)
    {
        return RegisterPair::AF;
    }
    throw InternalErrorException();
}

constexpr RegisterPair ssToRegister(const uint8 ss)
{
    if (ss == 0b00)
    {
        return RegisterPair::BC;
    }
    if (ss == 0b01)
    {
        return RegisterPair::DE;
    }
    if (ss == 0b10)
    {
        return RegisterPair::HL;
    }
    if (ss == 0b11)
    {
        return RegisterPair::SP;
    }
    throw InternalErrorException();
}

constexpr bool ccMatch(const uint8 cc, const Registers& registers)
{
    if (cc == 0b00)
    {
        return !registers.haveZeroFlag();
    }
    if (cc == 0b01)
    {
        return registers.haveZeroFlag();
    }
    if (cc == 0b10)
    {
        return !registers.haveCarryOnBit7();
    }
    if (cc == 0b11)
    {
        return registers.haveCarryOnBit7();
    }
    throw InternalErrorException();
}

std::string_view ccToString(const uint8 cc)
{
    if (cc == 0b00)
    {
        return "nz";
    }
    if (cc == 0b01)
    {
        return "z";
    }
    if (cc == 0b10)
    {
        return "nc";
    }
    if (cc == 0b11)
    {
        return "c";
    }
    throw InternalErrorException();
}

uint8 restartBitsToLowAddress(const uint8 ttt)
{
    if (ttt == 0b000)
    {
        return 0x00;
    }
    if (ttt == 0b001)
    {
        return 0x08;
    }
    if (ttt == 0b010)
    {
        return 0x10;
    }
    if (ttt == 0b011)
    {
        return 0x18;
    }
    if (ttt == 0b100)
    {
        return 0x20;
    }
    if (ttt == 0b101)
    {
        return 0x28;
    }
    if (ttt == 0b110)
    {
        return 0x30;
    }
    if (ttt == 0b111)
    {
        return 0x38;
    }
    throw InternalErrorException();
}

uint8 takeIntegerFromPc(Registers& registers, const Memory& memory)
{
    const uint16 pc = registers.pc();
    const uint8 integer = memory.read(pc);
    registers.setPc(pc + 1);
    return integer;
}

uint16 takeAddressFromPc(Registers& registers, const Memory& memory)
{
    uint16 address = 0;
    setLow(address, takeIntegerFromPc(registers, memory));
    setHigh(address, takeIntegerFromPc(registers, memory));
    return address;
}

int8 takeSignedIntegerFromPc(Registers& registers, const Memory& memory)
{
    return std::bit_cast<int8>(takeIntegerFromPc(registers, memory));
}

uint16 takeOffsetAddressFromPc(Registers& registers, const Memory& memory)
{
    const int8 offset = takeSignedIntegerFromPc(registers, memory);
    const uint16 address = int_cast<uint16>(registers.pc() + offset);
    return address;
}

constexpr uint8 simulateUint8Overflow(const int32 value)
{
    // Simulate overflow when value is the result of addition or subtraction of two uint8 numbers.
    if (value < 0)
    {
        return int_cast<uint8>(value + 1 + 0xFF);
    }
    if (value > 0xFF)
    {
        return int_cast<uint8>(value - 1 - 0xFF);
    }
    return int_cast<uint8>(value);
}

constexpr uint16 simulateUint16Overflow(const int32 value)
{
    // Simulate overflow when value is the result of addition or subtraction of two uint16 numbers.
    if (value < 0)
    {
        return int_cast<uint16>(value + 1 + 0xFFFF);
    }
    if (value > 0xFFFF)
    {
        return int_cast<uint16>(value - 1 - 0xFFFF);
    }
    return int_cast<uint16>(value);
}

constexpr void executeAddition(const int32 a, const int32 b, Registers& registers)
{
    const int32 addition = a + b;
    const int32 additionLow = (a & 0x0F) + (b & 0x0F);
    const uint8 result = simulateUint8Overflow(addition);
    registers.set(Register::A, result);
    registers.setZeroFlag(result == 0);
    registers.setSubtractionFlag(false);
    registers.setCarryOnBit3(additionLow > 0x0F);
    registers.setCarryOnBit7(addition > 0xFF);
}

constexpr void executeAdditionWithCarry(const int32 a, const int32 b, Registers& registers)
{
    const int32 carry = registers.haveCarryOnBit7() ? 1 : 0;
    const int32 addition = a + b + carry;
    const int32 additionLow = (a & 0x0F) + (b & 0x0F) + carry;
    const uint8 result = simulateUint8Overflow(addition);
    registers.set(Register::A, result);
    registers.setZeroFlag(result == 0);
    registers.setSubtractionFlag(false);
    registers.setCarryOnBit3(additionLow > 0x0F);
    registers.setCarryOnBit7(addition > 0xFF);
}

constexpr void executeSubtraction(const int32 a, const int32 b, Registers& registers)
{
    const int32 subtraction = a - b;
    const int32 subtractionLow = (a & 0x0F) - (b & 0x0F);
    const uint8 result = simulateUint8Overflow(subtraction);
    registers.set(Register::A, result);
    registers.setZeroFlag(result == 0);
    registers.setCarryOnBit3(subtractionLow < 0);
    registers.setCarryOnBit7(subtraction < 0);
    registers.setSubtractionFlag(true);
}

constexpr void executeSubtractionWithCarry(const int32 a, const int32 b, Registers& registers)
{
    const int32 carry = registers.haveCarryOnBit7() ? 1 : 0;
    const int32 subtraction = a - b - carry;
    const int32 subtractionLow = (a & 0x0F) - (b & 0x0F) - carry;
    const uint8 result = simulateUint8Overflow(subtraction);
    registers.set(Register::A, result);
    registers.setZeroFlag(result == 0);
    registers.setCarryOnBit3(subtractionLow < 0);
    registers.setCarryOnBit7(subtraction < 0);
    registers.setSubtractionFlag(true);
}

constexpr void executeAnd(const uint8 a, const uint8 b, Registers& registers)
{
    const uint8 result = (a & b);
    registers.set(Register::A, result);
    registers.setZeroFlag(result == 0);
    registers.setCarryOnBit3(true);
    registers.setCarryOnBit7(false);
    registers.setSubtractionFlag(false);
}

constexpr void executeOr(const uint8 a, const uint8 b, Registers& registers)
{
    const uint8 result = (a | b);
    registers.set(Register::A, result);
    registers.setZeroFlag(result == 0);
    registers.setCarryOnBit3(false);
    registers.setCarryOnBit7(false);
    registers.setSubtractionFlag(false);
}

constexpr void executeXor(const uint8 a, const uint8 b, Registers& registers)
{
    const uint8 result = (a ^ b);
    registers.set(Register::A, result);
    registers.setZeroFlag(result == 0);
    registers.setCarryOnBit3(false);
    registers.setCarryOnBit7(false);
    registers.setSubtractionFlag(false);
}

constexpr void executeCompare(const int32 a, const int32 b, Registers& registers)
{
    const int32 difference = (a & 0xFF) - (b & 0xFF);
    const int32 differenceLow = (a & 0x0F) - (b & 0x0F);
    registers.setZeroFlag(difference == 0);
    registers.setCarryOnBit3(differenceLow < 0);
    registers.setCarryOnBit7(difference < 0);
    registers.setSubtractionFlag(true);
}

constexpr uint8 executeIncrement(const int32 a, Registers& registers)
{
    const int32 increment = a + 1;
    const uint8 result = simulateUint8Overflow(increment);
    registers.setZeroFlag(result == 0);
    registers.setCarryOnBit3((a & 0xF) == 0xF);
    registers.setSubtractionFlag(false);
    return result;
}

constexpr uint8 executeDecrement(const int32 a, Registers& registers)
{
    const int32 decrement = a - 1;
    const uint8 result = simulateUint8Overflow(decrement);
    registers.setZeroFlag(result == 0);
    registers.setCarryOnBit3((a & 0xF) == 0x0);
    registers.setSubtractionFlag(true);
    return result;
}

void executePush(const uint16 valueToPush, const ProgramState& state)
{
    const uint16 sp = state.registers.sp();

    state.memory.write(sp - 1, getHigh(valueToPush));
    state.memory.write(sp - 2, getLow(valueToPush));
    state.registers.setSp(sp - 2);
}

void executePop(const RegisterPair registerToFill, const ProgramState& state)
{
    const uint16 sp = state.registers.sp();

    uint16 value = 0;
    setLow(value, state.memory.read(sp));
    setHigh(value, state.memory.read(sp + 1));

    state.registers.set(registerToFill, value);
    state.registers.setSp(sp + 2);
}

constexpr uint8 rotateLeft(const uint8 value, Registers& registers)
{
    const bool bit7 = isBitTrue(value, 7);

    uint8 result = int_cast<uint8>((value << 1) & 0xFF);
    setBit(result, 0, bit7);

    registers.resetFlags();
    registers.setCarryOnBit7(bit7);

    return result;
}

constexpr uint8 rotateRight(const uint8 value, Registers& registers)
{
    const bool bit0 = isBitTrue(value, 0);

    uint8 result = int_cast<uint8>((value >> 1) & 0xFF);
    setBit(result, 7, bit0);

    registers.resetFlags();
    registers.setCarryOnBit7(bit0);

    return result;
}

constexpr uint8 rotateLeftWithCarry(const uint8 value, Registers& registers)
{
    const bool carry = registers.haveCarryOnBit7();
    const bool bit7 = isBitTrue(value, 7);

    uint8 result = int_cast<uint8>((value << 1) & 0xFF);
    setBit(result, 0, carry);

    registers.resetFlags();
    registers.setCarryOnBit7(bit7);

    return result;
}

constexpr uint8 rotateRightWithCarry(const uint8 value, Registers& registers)
{
    const bool carry = registers.haveCarryOnBit7();
    const bool bit0 = isBitTrue(value, 0);

    uint8 result = int_cast<uint8>((value >> 1) & 0xFF);
    setBit(result, 7, carry);

    registers.resetFlags();
    registers.setCarryOnBit7(bit0);

    return result;
}

constexpr uint8 shiftLeft(const uint8 value, Registers& registers)
{
    const bool bit7 = isBitTrue(value, 7);

    const uint8 result = int_cast<uint8>((value << 1) & 0xFF);

    registers.resetFlags();
    registers.setZeroFlag(result == 0);
    registers.setCarryOnBit7(bit7);

    return result;
}

constexpr uint8 shiftRight(const uint8 value, Registers& registers)
{
    const bool bit0 = isBitTrue(value, 0);

    const uint8 result = int_cast<uint8>((value >> 1) & 0xFF);

    registers.resetFlags();
    registers.setZeroFlag(result == 0);
    registers.setCarryOnBit7(bit0);

    return result;
}

constexpr uint8 shiftRightKeepBit7(const uint8 value, Registers& registers)
{
    const bool bit0 = isBitTrue(value, 0);
    const bool bit7 = isBitTrue(value, 7);

    uint8 result = int_cast<uint8>((value >> 1) & 0xFF);
    setBit(result, 7, bit7);

    registers.resetFlags();
    registers.setZeroFlag(result == 0);
    registers.setCarryOnBit7(bit0);

    return result;
}

constexpr uint8 swapLowAndHigh(const uint8 value, Registers& registers)
{
    const uint8 low = (value & 0x0F);
    const uint8 high = (value & 0xF0);
    const uint8 result = int_cast<uint8>((low << 4) | (high >> 4));

    registers.resetFlags();
    registers.setZeroFlag(result == 0);

    return result;
}

Cycles instructionNoOperation(const ProgramState& state)
{
    if (state.log)
    {
        state.log() << "noop";
    }
    return 1;
}

Cycles instructionStop(const ProgramState& state)
{
    state.registers.setProcessorStopped(true);
    if (state.log)
    {
        state.log() << "stop";
    }
    return 1;
}

Cycles instructionHalt(const ProgramState& state)
{
    state.registers.setProcessorHalt(state.registers.pc());
    if (state.log)
    {
        state.log() << "halt";
    }
    return 1;
}

Cycles instructionInvalid(const ProgramState& state)
{
    const std::string message = "Invalid instruction: " + toBinary(state.instruction);
    throw UnsupportedException(message);
}

Cycles instructionDisableInterrupts(const ProgramState& state)
{
    state.registers.setInterruptsEnabled(false);
    if (state.log)
    {
        state.log() << "DI (disable interrupts)";
    }
    return 1;
}

Cycles instructionEnableInterrupts(const ProgramState& state)
{
    state.registers.setInterruptsEnabled(true);
    if (state.log)
    {
        state.log() << "EI (enable interrupts)";
    }
    return 1;
}

Cycles instructionDecimalAdjustRegisterA(const ProgramState& state)
{
    // From https://forums.nesdev.com/viewtopic.php?t=15944
    uint8 a = state.registers.get(Register::A);

    if (!state.registers.haveSubtractionFlag())
    {
        // after an addition, adjust if (half-)carry occurred or if result is out of bounds
        if (state.registers.haveCarryOnBit7() || a > 0x99)
        {
            a += 0x60;
            state.registers.setCarryOnBit7(true);
        }
        if (state.registers.haveCarryOnBit3() || (a & 0x0f) > 0x09)
        {
            a += 0x6;
        }
    }
    else
    {
        // after a subtraction, only adjust if (half-)carry occurred
        if (state.registers.haveCarryOnBit7())
        {
            a -= 0x60;
        }
        if (state.registers.haveCarryOnBit3())
        {
            a -= 0x6;
        }
    }

    state.registers.set(Register::A, a);
    state.registers.setZeroFlag(a == 0);
    state.registers.setCarryOnBit3(false);

    if (state.log)
    {
        state.log() << "daa";
    }
    return 1;
}

Cycles instructionLoadRegister8WithRegister8(const ProgramState& state)
{
    const uint8 sourceBits = getOffsetBits<3, 0>(state.instruction);
    const Register source = bitsToRegister(sourceBits);
    const uint8 targetBits = getOffsetBits<3, 3>(state.instruction);
    const Register target = bitsToRegister(targetBits);
    state.registers.set(target, state.registers.get(source));
    if (state.log)
    {
        state.log() << "ld" << toString(target) << toString(source);
    }
    return 1;
}

Cycles instructionLoadRegister8WithAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.get(RegisterPair::HL);
    const uint8 targetBits = getOffsetBits<3, 3>(state.instruction);
    const Register target = bitsToRegister(targetBits);
    const uint8 value = state.memory.read(address);
    state.registers.set(target, value);
    if (state.log)
    {
        state.log() << "ld" << toString(target) << "[hl]";
    }
    return 2;
}

Cycles instructionLoadRegisterAWithAddressBc(const ProgramState& state)
{
    const uint16 address = state.registers.get(RegisterPair::BC);
    const uint8 value = state.memory.read(address);
    state.registers.set(Register::A, value);
    if (state.log)
    {
        state.log() << "ld a, [bc]" << ("n=" + toHex(address, "$"));
    }
    return 3;
}

Cycles instructionLoadRegisterAWithAddressDe(const ProgramState& state)
{
    const uint16 address = state.registers.get(RegisterPair::DE);
    const uint8 value = state.memory.read(address);
    state.registers.set(Register::A, value);
    if (state.log)
    {
        state.log() << "ld a, [de]";
    }
    return 3;
}

Cycles instructionLoadRegisterAWithAddressHlIncrementHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 value = state.memory.read(address);
    state.registers.set(Register::A, value);
    state.registers.setHl(state.registers.hl() + 1);
    if (state.log)
    {
        state.log() << "ld a, [hl+]";
    }
    return 2;
}

Cycles instructionLoadRegisterAWithAddressHlDecrementHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 value = state.memory.read(address);
    state.registers.set(Register::A, value);
    state.registers.setHl(state.registers.hl() - 1);
    if (state.log)
    {
        state.log() << "ld a, [hl-]";
    }
    return 2;
}

Cycles instructionLoadAddressBcWithRegisterA(const ProgramState& state)
{
    const uint16 address = state.registers.get(RegisterPair::BC);
    const uint8 value = state.registers.get(Register::A);
    state.memory.write(address, value);
    if (state.log)
    {
        state.log() << "ld [bc], a";
    }
    return 2;
}

Cycles instructionLoadRegister8WithValue(const ProgramState& state)
{
    const uint8 registerBits = getOffsetBits<3, 3>(state.instruction);
    const Register target = bitsToRegister(registerBits);
    const uint8 value = takeIntegerFromPc(state.registers, state.memory);
    state.registers.set(target, value);
    if (state.log)
    {
        state.log() << "ld" << toString(target) << toHex(value, "$");
    }
    return 2;
}

Cycles instructionLoadAddressPcWithRegisterSp(const ProgramState& state)
{
    const uint16 address = takeAddressFromPc(state.registers, state.memory);
    const uint16 sp = state.registers.sp();
    state.memory.write(address, getLow(sp));
    state.memory.write(address + 1, getHigh(sp));
    if (state.log)
    {
        state.log() << "ld [n], sp" << ("n=" + toHex(address, "$"));
    }
    return 5;
}

Cycles instructionLoadAddressPcWithRegisterA(const ProgramState& state)
{
    const uint16 address = takeAddressFromPc(state.registers, state.memory);
    const uint8 value = state.registers.get(Register::A);
    state.memory.write(address, value);
    if (state.log)
    {
        state.log() << "ld [n], a" << ("n=" + toHex(address, "$"));
    }
    return 4;
}

Cycles instructionLoadAddressHlWithRegisterADecrementHl(const ProgramState& state)
{
    const uint16 address = state.registers.get(RegisterPair::HL);
    const uint8 value = state.registers.get(Register::A);
    state.memory.write(address, value);
    state.registers.setHl(state.registers.hl() - 1);
    if (state.log)
    {
        state.log() << "ld [hl-], a";
    }
    return 2;
}

Cycles instructionLoadRegister16WithValue(const ProgramState& state)
{
    const uint8 dd = getOffsetBits<2, 4>(state.instruction);
    const RegisterPair target = ddToRegister(dd);
    const uint16 value = takeAddressFromPc(state.registers, state.memory);
    state.registers.set(target, value);
    if (state.log)
    {
        state.log() << "ld" << toString(target) << toHex(value, "$");
    }
    return 3;
}

Cycles instructionLoadAddressDeWithRegisterA(const ProgramState& state)
{
    const uint16 address = state.registers.get(RegisterPair::DE);
    const uint8 value = state.registers.get(Register::A);
    state.memory.write(address, value);
    if (state.log)
    {
        state.log() << "ld [de], a";
    }
    return 2;
}

Cycles instructionLoadAddressHlWithRegister8(const ProgramState& state)
{
    const uint16 address = state.registers.get(RegisterPair::HL);
    const uint8 targetBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(targetBits);
    state.memory.write(address, state.registers.get(target));
    if (state.log)
    {
        state.log() << "ld [hl]" << toString(target);
    }
    return 2;
}

Cycles instructionLoadAddressHlWithRegisterAIncrementHl(const ProgramState& state)
{
    const uint16 address = state.registers.get(RegisterPair::HL);
    const uint8 value = state.registers.get(Register::A);
    state.memory.write(address, value);
    state.registers.setHl(state.registers.hl() + 1);
    if (state.log)
    {
        state.log() << "ld [hl+], a";
    }
    return 2;
}

Cycles instructionLoadAddressHlWithValue(const ProgramState& state)
{
    const uint16 address = state.registers.get(RegisterPair::HL);
    const uint8 value = takeIntegerFromPc(state.registers, state.memory);
    state.memory.write(address, value);
    if (state.log)
    {
        state.log() << "ld [hl]" << toHex(value, "$");
    }
    return 3;
}

Cycles instructionLoadPcOffsetAddressWithRegisterA(const ProgramState& state)
{
    const uint8 pc = takeIntegerFromPc(state.registers, state.memory);
    uint16 address = 0xFF00;
    setLow(address, pc);
    const uint8 value = state.registers.get(Register::A);
    state.memory.write(address, value);
    if (state.log)
    {
        state.log() << "ld [n], a" << ("n=" + toHex(address, "$"));
    }
    return 3;
}

Cycles instructionLoadRegisterAWithAddressPc(const ProgramState& state)
{
    const uint16 address = takeAddressFromPc(state.registers, state.memory);
    const uint8 value = state.memory.read(address);
    state.registers.set(Register::A, value);
    if (state.log)
    {
        state.log() << "ld a, [n]" << ("n=" + toHex(address, "$"));
    }
    return 4;
}

Cycles instructionLoadRegisterAWithPcOffsetAddress(const ProgramState& state)
{
    const uint8 pc = takeIntegerFromPc(state.registers, state.memory);
    uint16 address = 0xFF00;
    setLow(address, pc);
    const uint8 value = state.memory.read(address);
    state.registers.set(Register::A, value);
    if (state.log)
    {
        state.log() << "ld a, [n]" << ("n=" + toHex(address, "$"));
    }
    return 3;
}

Cycles instructionLoadRegisterAWithOffsetAddressC(const ProgramState& state)
{
    const uint8 c = state.registers.get(Register::C);
    uint16 address = 0xFF00;
    setLow(address, c);
    const uint8 value = state.memory.read(address);
    state.registers.set(Register::A, value);
    if (state.log)
    {
        state.log() << "ld a, [c]" << ("n=" + toHex(address, "$"));
    }
    return 2;
}

Cycles instructionLoadOffsetAddressCWithRegisterA(const ProgramState& state)
{
    uint16 address = 0xFF00;
    setLow(address, state.registers.get(Register::C));
    const uint8 value = state.registers.get(Register::A);
    state.memory.write(address, value);
    if (state.log)
    {
        state.log() << "ld [c], a" << ("n=" + toHex(address, "$"));
    }
    return 2;
}

Cycles instructionLoadRegisterSpWithRegisterHl(const ProgramState& state)
{
    state.registers.setSp(state.registers.hl());
    if (state.log)
    {
        state.log() << "ld sp, hl";
    }
    return 2;
}

Cycles instructionLoadRegisterHlWithSpPlusPcOffset(const ProgramState& state)
{
    const int32 sp = state.registers.sp();
    const int8 offset = takeSignedIntegerFromPc(state.registers, state.memory);

    const int32 differenceLow = (sp & 0x0F) + (offset & 0x0F);
    const int32 differenceHigh = (sp & 0xFF) + (offset & 0xFF);
    const int32 difference = sp + offset;
    const uint16 result = simulateUint16Overflow(difference);

    state.registers.setHl(result);
    state.registers.setZeroFlag(false);
    state.registers.setCarryOnBit3(differenceLow > 0x0F || differenceLow < 0);
    state.registers.setCarryOnBit7(differenceHigh > 0xFF || differenceHigh < 0);
    state.registers.setSubtractionFlag(false);

    if (state.log)
    {
        state.log() << "ldhl" << offset;
    }
    return 3;
}

Cycles instructionAdditionRegisterAWithRegister8(const ProgramState& state)
{
    const uint8 targetBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(targetBits);
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.registers.get(target);
    executeAddition(a, b, state.registers);
    if (state.log)
    {
        state.log() << "add" << toString(Register::A) << toString(target);
    }
    return 1;
}

Cycles instructionAdditionRegisterAWithAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.memory.read(address);
    executeAddition(a, b, state.registers);
    if (state.log)
    {
        state.log() << "add" << toString(Register::A) << toHex(address);
    }
    return 2;
}

Cycles instructionAdditionRegisterAWithValue(const ProgramState& state)
{
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = takeIntegerFromPc(state.registers, state.memory);
    executeAddition(a, b, state.registers);
    if (state.log)
    {
        state.log() << "add" << toString(Register::A) << toHex(b, "$");
    }
    return 2;
}

Cycles instructionAdditionRegister16WithRegisterHl(const ProgramState& state)
{
    const uint8 ss = getOffsetBits<2, 4>(state.instruction);
    const RegisterPair target = ssToRegister(ss);
    const int32 a = state.registers.get(target);
    const int32 b = state.registers.get(RegisterPair::HL);
    const int32 addition = a + b;
    const int32 additionLow = (a & 0x0FFF) + (b & 0x0FFF);
    const uint16 result = simulateUint16Overflow(addition);
    state.registers.setHl(result);
    state.registers.setCarryOnBit3(additionLow > 0x0FFF);
    state.registers.setCarryOnBit7(addition > 0xFFFF);
    state.registers.setSubtractionFlag(false);
    if (state.log)
    {
        state.log() << "add 16" << toString(RegisterPair::HL) << toString(target);
    }
    return 2;
}

Cycles instructionAdditionRegisterSpWithSignedPc(const ProgramState& state)
{
    const int32 a = state.registers.sp();
    const int8 b = takeSignedIntegerFromPc(state.registers, state.memory);
    const int32 addition = a + b;
    const int32 additionLow = (a & 0x0F) + (b & 0x0F);
    const int32 additionHigh = (a & 0xFF) + (b & 0xFF);
    const uint16 result = simulateUint16Overflow(addition);
    state.registers.setSp(result);
    state.registers.setZeroFlag(false);
    state.registers.setCarryOnBit3(additionLow > 0x0F);
    state.registers.setCarryOnBit7(additionHigh > 0xFF);
    state.registers.setSubtractionFlag(false);
    if (state.log)
    {
        state.log() << "add 16" << toString(RegisterPair::SP) << b;
    }
    return 4;
}

Cycles instructionAdditionWithCarryRegisterAWithRegister8(const ProgramState& state)
{
    const uint8 targetBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(targetBits);
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.registers.get(target);
    executeAdditionWithCarry(a, b, state.registers);
    if (state.log)
    {
        state.log() << "adc" << toString(Register::A) << toString(target);
    }
    return 1;
}

Cycles instructionAdditionWithCarryRegisterAWithAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.memory.read(address);
    executeAdditionWithCarry(a, b, state.registers);
    if (state.log)
    {
        state.log() << "adc" << toString(Register::A) << toHex(address);
    }
    return 2;
}

Cycles instructionAdditionWithCarryRegisterAWithValue(const ProgramState& state)
{
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = takeIntegerFromPc(state.registers, state.memory);
    executeAdditionWithCarry(a, b, state.registers);
    if (state.log)
    {
        state.log() << "adc" << toString(Register::A) << toHex(b, "$");
    }
    return 2;
}

Cycles instructionSubtractRegisterAWithRegister8(const ProgramState& state)
{
    const uint8 targetBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(targetBits);
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.registers.get(target);
    executeSubtraction(a, b, state.registers);
    if (state.log)
    {
        state.log() << "sub" << toString(Register::A) << toString(target);
    }
    return 1;
}

Cycles instructionSubtractRegisterAWithAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.memory.read(address);
    executeSubtraction(a, b, state.registers);
    if (state.log)
    {
        state.log() << "sub" << toString(Register::A) << toHex(address);
    }
    return 2;
}

Cycles instructionSubtractRegisterAWithValue(const ProgramState& state)
{
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = takeIntegerFromPc(state.registers, state.memory);
    executeSubtraction(a, b, state.registers);
    if (state.log)
    {
        state.log() << "sub" << toString(Register::A) << toHex(b, "$");
    }
    return 2;
}

Cycles instructionSubtractWithCarryRegisterAWithRegister8(const ProgramState& state)
{
    const uint8 targetBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(targetBits);
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.registers.get(target);
    executeSubtractionWithCarry(a, b, state.registers);
    if (state.log)
    {
        state.log() << "sbc" << toString(Register::A) << toString(target);
    }
    return 1;
}

Cycles instructionSubtractWithCarryRegisterAWithAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.memory.read(address);
    executeSubtractionWithCarry(a, b, state.registers);
    if (state.log)
    {
        state.log() << "sbc" << toString(Register::A) << toHex(address);
    }
    return 2;
}

Cycles instructionSubtractWithCarryRegisterAWithValue(const ProgramState& state)
{
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = takeIntegerFromPc(state.registers, state.memory);
    executeSubtractionWithCarry(a, b, state.registers);
    if (state.log)
    {
        state.log() << "sbc" << toString(Register::A) << toHex(b, "$");
    }
    return 2;
}

Cycles instructionAndRegisterAWithRegister8(const ProgramState& state)
{
    const uint8 targetBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(targetBits);
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.registers.get(target);
    executeAnd(a, b, state.registers);
    if (state.log)
    {
        state.log() << "and" << toString(Register::A) << toString(target);
    }
    return 1;
}

Cycles instructionAndRegisterAWithAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.memory.read(address);
    executeAnd(a, b, state.registers);
    if (state.log)
    {
        state.log() << "and" << toString(Register::A) << toHex(address);
    }
    return 2;
}

Cycles instructionAndRegisterAWithValue(const ProgramState& state)
{
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = takeIntegerFromPc(state.registers, state.memory);
    executeAnd(a, b, state.registers);
    if (state.log)
    {
        state.log() << "and" << toString(Register::A) << toHex(b, "$");
    }
    return 2;
}

Cycles instructionOrRegisterAWithRegister8(const ProgramState& state)
{
    const uint8 targetBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(targetBits);
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.registers.get(target);
    executeOr(a, b, state.registers);
    if (state.log)
    {
        state.log() << "or" << toString(Register::A) << toString(target);
    }
    return 1;
}

Cycles instructionOrRegisterAWithAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.memory.read(address);
    executeOr(a, b, state.registers);
    if (state.log)
    {
        state.log() << "or" << toString(Register::A) << toHex(address);
    }
    return 2;
}

Cycles instructionOrRegisterAWithValue(const ProgramState& state)
{
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = takeIntegerFromPc(state.registers, state.memory);
    executeOr(a, b, state.registers);
    if (state.log)
    {
        state.log() << "or" << toString(Register::A) << toHex(b, "$");
    }
    return 2;
}

Cycles instructionXorRegisterAWithRegister8(const ProgramState& state)
{
    const uint8 targetBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(targetBits);
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.registers.get(target);
    executeXor(a, b, state.registers);
    if (state.log)
    {
        state.log() << "xor" << toString(Register::A) << toString(target);
    }
    return 1;
}

Cycles instructionXorRegisterAWithAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.memory.read(address);
    executeXor(a, b, state.registers);
    if (state.log)
    {
        state.log() << "xor" << toString(Register::A) << toHex(address);
    }
    return 2;
}

Cycles instructionXorRegisterAWithValue(const ProgramState& state)
{
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = takeIntegerFromPc(state.registers, state.memory);
    executeXor(a, b, state.registers);
    if (state.log)
    {
        state.log() << "xor" << toString(Register::A) << toHex(b, "$");
    }
    return 2;
}

Cycles instructionCompareRegisterAWithRegister8(const ProgramState& state)
{
    const uint8 targetBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(targetBits);
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.registers.get(target);
    executeCompare(a, b, state.registers);
    if (state.log)
    {
        state.log() << "cp" << toString(Register::A) << toString(target);
    }
    return 1;
}

Cycles instructionCompareRegisterAWithAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = state.memory.read(address);
    executeCompare(a, b, state.registers);
    if (state.log)
    {
        state.log() << "cp" << toString(Register::A) << toHex(address);
    }
    return 2;
}

Cycles instructionCompareRegisterAWithValue(const ProgramState& state)
{
    const uint8 a = state.registers.get(Register::A);
    const uint8 b = takeIntegerFromPc(state.registers, state.memory);
    executeCompare(a, b, state.registers);
    if (state.log)
    {
        state.log() << "cp" << toString(Register::A) << toHex(b, "$");
    }
    return 2;
}

Cycles instructionIncrementRegister8(const ProgramState& state)
{
    const uint8 registerBits = getOffsetBits<3, 3>(state.instruction);
    const Register target = bitsToRegister(registerBits);
    const uint8 value = state.registers.get(target);
    const uint8 result = executeIncrement(value, state.registers);
    state.registers.set(target, result);
    if (state.log)
    {
        state.log() << "inc" << toString(target);
    }
    return 1;
}

Cycles instructionDecrementRegister8(const ProgramState& state)
{
    const uint8 registerBits = getOffsetBits<3, 3>(state.instruction);
    const Register target = bitsToRegister(registerBits);
    const uint8 value = state.registers.get(target);
    const uint8 result = executeDecrement(value, state.registers);
    state.registers.set(target, result);
    if (state.log)
    {
        state.log() << "dec" << toString(target);
    }
    return 1;
}

Cycles instructionIncrementRegister16(const ProgramState& state)
{
    const uint8 ss = getOffsetBits<2, 4>(state.instruction);
    const RegisterPair target = ssToRegister(ss);
    const int32 result = state.registers.get(target) + 1;
    state.registers.set(target, simulateUint16Overflow(result));
    if (state.log)
    {
        state.log() << "inc 16" << toString(target);
    }
    return 2;
}

Cycles instructionDecrementRegister16(const ProgramState& state)
{
    const uint8 ss = getOffsetBits<2, 4>(state.instruction);
    const RegisterPair target = ssToRegister(ss);
    const int32 result = state.registers.get(target) - 1;
    state.registers.set(target, simulateUint16Overflow(result));
    if (state.log)
    {
        state.log() << "dec 16" << toString(target);
    }
    return 2;
}

Cycles instructionIncrementAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 value = state.memory.read(address);
    const uint8 result = executeIncrement(value, state.registers);
    state.memory.write(address, result);
    if (state.log)
    {
        state.log() << "inc" << toHex(address);
    }
    return 3;
}

Cycles instructionDecrementAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 value = state.memory.read(address);
    const uint8 result = executeDecrement(value, state.registers);
    state.memory.write(address, result);
    if (state.log)
    {
        state.log() << "dec" << toHex(address);
    }
    return 3;
}

Cycles instructionNegateRegisterA(const ProgramState& state)
{
    const uint8 value = ~state.registers.get(Register::A);
    state.registers.set(Register::A, value);
    state.registers.setSubtractionFlag(true);
    state.registers.setCarryOnBit3(true);
    if (state.log)
    {
        state.log() << "cpl";
    }
    return 1;
}

Cycles instructionRotateLeftRegisterA(const ProgramState& state)
{
    const uint8 a = state.registers.get(Register::A);
    const uint8 rotated = rotateLeft(a, state.registers);
    state.registers.set(Register::A, rotated);
    if (state.log)
    {
        state.log() << "rlca";
    }
    return 1;
}

Cycles instructionRotateLeftWithCarryRegisterA(const ProgramState& state)
{
    const uint8 a = state.registers.get(Register::A);
    const uint8 rotated = rotateLeftWithCarry(a, state.registers);
    state.registers.set(Register::A, rotated);
    if (state.log)
    {
        state.log() << "rla";
    }
    return 1;
}

Cycles instructionRotateRightRegisterA(const ProgramState& state)
{
    const uint8 a = state.registers.get(Register::A);
    const uint8 rotated = rotateRight(a, state.registers);
    state.registers.set(Register::A, rotated);
    if (state.log)
    {
        state.log() << "rrca";
    }
    return 1;
}

Cycles instructionRotateRightWithCarryRegisterA(const ProgramState& state)
{
    const uint8 a = state.registers.get(Register::A);
    const uint8 rotated = rotateRightWithCarry(a, state.registers);
    state.registers.set(Register::A, rotated);
    if (state.log)
    {
        state.log() << "rra";
    }
    return 1;
}

Cycles instructionNegateCarryFlag(const ProgramState& state)
{
    state.registers.setCarryOnBit7(!state.registers.haveCarryOnBit7());
    state.registers.setSubtractionFlag(false);
    state.registers.setCarryOnBit3(false);
    if (state.log)
    {
        state.log() << "ccf";
    }
    return 1;
}

Cycles instructionSetCarryFlag(const ProgramState& state)
{
    state.registers.setCarryOnBit7(true);
    state.registers.setSubtractionFlag(false);
    state.registers.setCarryOnBit3(false);
    if (state.log)
    {
        state.log() << "scf";
    }
    return 1;
}

Cycles instructionJumpAddressPc(const ProgramState& state)
{
    const uint16 address = takeAddressFromPc(state.registers, state.memory);
    state.registers.setPc(address);
    if (state.log)
    {
        state.log() << "jp" << toHex(address);
    }
    return 12;
}

Cycles instructionJumpAddressPcConditional(const ProgramState& state)
{
    const uint16 newPc = takeAddressFromPc(state.registers, state.memory);
    const uint8 cc = getOffsetBits<2, 3>(state.instruction);
    const bool jump = ccMatch(cc, state.registers);
    if (jump)
    {
        state.registers.setPc(newPc);
    }
    if (state.log)
    {
        state.log() << "jp" << ccToString(cc) << toHex(newPc);
    }
    return jump ? 4 : 3;
}

Cycles instructionJumpPcOffsetAddress(const ProgramState& state)
{
    const uint16 newPc = takeOffsetAddressFromPc(state.registers, state.memory);
    state.registers.setPc(newPc);
    if (state.log)
    {
        state.log() << "jr n" << toHex(newPc);
    }
    return 3;
}

Cycles instructionJumpPcOffsetAddressConditional(const ProgramState& state)
{
    const uint16 newPc = takeOffsetAddressFromPc(state.registers, state.memory);
    const uint8 cc = getOffsetBits<2, 3>(state.instruction);
    const bool jump = ccMatch(cc, state.registers);
    if (jump)
    {
        state.registers.setPc(newPc);
    }
    if (state.log)
    {
        state.log() << "jr" << ccToString(cc) << toHex(newPc);
    }
    return jump ? 3 : 2;
}

Cycles instructionJumpRegisterHl(const ProgramState& state)
{
    const uint16 newPc = state.registers.hl();
    state.registers.setPc(newPc);
    if (state.log)
    {
        state.log() << "jp (HL)" << toHex(newPc);
    }
    return 1;
}

Cycles instructionCallAddressPc(const ProgramState& state)
{
    const uint16 address = takeAddressFromPc(state.registers, state.memory);
    const uint16 pc = state.registers.pc();
    executePush(pc, state);
    state.registers.setPc(address);
    if (state.log)
    {
        state.log() << "call" << toHex(address);
    }
    return 6;
}

Cycles instructionCallAddressPcConditional(const ProgramState& state)
{
    const uint8 cc = getOffsetBits<2, 3>(state.instruction);
    const uint16 address = takeAddressFromPc(state.registers, state.memory);
    const bool jump = ccMatch(cc, state.registers);
    if (jump)
    {
        const uint16 pc = state.registers.pc();
        executePush(pc, state);
        state.registers.setPc(address);
    }
    if (state.log)
    {
        state.log() << "call" << ccToString(cc) << toHex(address);
    }
    return jump ? 6 : 3;
}

Cycles instructionReturn(const ProgramState& state)
{
    executePop(RegisterPair::PC, state);
    if (state.log)
    {
        state.log() << "ret";
    }
    return 4;
}

Cycles instructionReturnConditional(const ProgramState& state)
{
    const uint8 cc = getOffsetBits<2, 3>(state.instruction);
    const bool jump = ccMatch(cc, state.registers);
    if (jump)
    {
        executePop(RegisterPair::PC, state);
    }
    if (state.log)
    {
        state.log() << "ret" << ccToString(cc);
    }
    return jump ? 5 : 2;
}

Cycles instructionReturnEnableInterrupts(const ProgramState& state)
{
    executePop(RegisterPair::PC, state);
    state.registers.setInterruptsEnabled(true);
    if (state.log)
    {
        state.log() << "reti";
    }
    return 4;
}

Cycles instructionRestart(const ProgramState& state)
{
    const uint8 rstBits = getOffsetBits<3, 3>(state.instruction);
    uint16 address = 0;
    setLow(address, restartBitsToLowAddress(rstBits));
    setHigh(address, 0x0);

    const uint16 pc = state.registers.pc();
    executePush(pc, state);
    state.registers.setPc(address);

    if (state.log)
    {
        state.log() << "rst" << toHex(address);
    }
    return 4;
}

Cycles instructionPushRegister16(const ProgramState& state)
{
    const uint8 qq = getOffsetBits<2, 4>(state.instruction);
    const RegisterPair target = qqToRegister(qq);
    const uint16 value = state.registers.get(target);
    executePush(value, state);
    if (state.log)
    {
        state.log() << "push" << toString(target);
    }
    return 4;
}

Cycles instructionPopRegister16(const ProgramState& state)
{
    const uint8 qq = getOffsetBits<2, 4>(state.instruction);
    const RegisterPair target = qqToRegister(qq);
    executePop(target, state);
    if (state.log)
    {
        state.log() << "pop" << toString(target);
    }
    return 3;
}

Cycles instructionRotateLeftRegister8(const ProgramState& state)
{
    const uint8 registerBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(registerBits);
    const uint8 value = state.registers.get(target);
    const uint8 result = rotateLeft(value, state.registers);
    state.registers.set(target, result);
    state.registers.setZeroFlag(result == 0);
    if (state.log)
    {
        state.log() << "rlc" << toString(target);
    }
    return 2;
}

Cycles instructionRotateAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 value = state.memory.read(address);
    const uint8 result = rotateLeft(value, state.registers);
    state.memory.write(address, result);
    state.registers.setZeroFlag(result == 0);
    if (state.log)
    {
        state.log() << "rlc" << toHex(address);
    }
    return 4;
}

Cycles instructionRotateLeftWithCarryRegister8(const ProgramState& state)
{
    const uint8 registerBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(registerBits);
    const uint8 value = state.registers.get(target);
    const uint8 result = rotateLeftWithCarry(value, state.registers);
    state.registers.set(target, result);
    state.registers.setZeroFlag(result == 0);
    if (state.log)
    {
        state.log() << "rl" << toString(target);
    }
    return 2;
}

Cycles instructionRotateLeftWithCarryAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 value = state.memory.read(address);
    const uint8 result = rotateLeftWithCarry(value, state.registers);
    state.memory.write(address, result);
    state.registers.setZeroFlag(result == 0);
    if (state.log)
    {
        state.log() << "rl" << toHex(address);
    }
    return 4;
}

Cycles instructionRotateRightRegister8(const ProgramState& state)
{
    const uint8 registerBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(registerBits);
    const uint8 value = state.registers.get(target);
    const uint8 result = rotateRight(value, state.registers);
    state.registers.set(target, result);
    state.registers.setZeroFlag(result == 0);
    if (state.log)
    {
        state.log() << "rrc" << toString(target);
    }
    return 2;
}

Cycles instructionRotateRightAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 value = state.memory.read(address);
    const uint8 result = rotateRight(value, state.registers);
    state.memory.write(address, result);
    state.registers.setZeroFlag(result == 0);
    if (state.log)
    {
        state.log() << "rrc" << toHex(address);
    }
    return 4;
}

Cycles instructionRotateRightWithCarryRegister8(const ProgramState& state)
{
    const uint8 registerBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(registerBits);
    const uint8 value = state.registers.get(target);
    const uint8 result = rotateRightWithCarry(value, state.registers);
    state.registers.set(target, result);
    state.registers.setZeroFlag(result == 0);
    if (state.log)
    {
        state.log() << "rr" << toString(target);
    }
    return 2;
}

Cycles instructionRotateRightWithCarryAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 value = state.memory.read(address);
    const uint8 result = rotateRightWithCarry(value, state.registers);
    state.memory.write(address, result);
    state.registers.setZeroFlag(result == 0);
    if (state.log)
    {
        state.log() << "rr" << toHex(address);
    }
    return 4;
}

Cycles instructionShiftLeftRegister8(const ProgramState& state)
{
    const uint8 registerBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(registerBits);
    const uint8 value = state.registers.get(target);
    const uint8 result = shiftLeft(value, state.registers);
    state.registers.set(target, result);
    if (state.log)
    {
        state.log() << "sla" << toString(target);
    }
    return 2;
}

Cycles instructionShiftLeftAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 value = state.memory.read(address);
    const uint8 result = shiftLeft(value, state.registers);
    state.memory.write(address, result);
    if (state.log)
    {
        state.log() << "sla" << toHex(address);
    }
    return 4;
}

Cycles instructionShiftRightKeepBit7Register8(const ProgramState& state)
{
    const uint8 registerBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(registerBits);
    const uint8 value = state.registers.get(target);
    const uint8 result = shiftRightKeepBit7(value, state.registers);
    state.registers.set(target, result);
    if (state.log)
    {
        state.log() << "sra" << toString(target);
    }
    return 2;
}

Cycles instructionShiftRightKeepBit7AddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 value = state.memory.read(address);
    const uint8 result = shiftRightKeepBit7(value, state.registers);
    state.memory.write(address, result);
    if (state.log)
    {
        state.log() << "sra" << toHex(address);
    }
    return 4;
}

Cycles instructionShiftRightRegister8(const ProgramState& state)
{
    const uint8 registerBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(registerBits);
    const uint8 value = state.registers.get(target);
    const uint8 result = shiftRight(value, state.registers);
    state.registers.set(target, result);
    if (state.log)
    {
        state.log() << "srl" << toString(target);
    }
    return 2;
}

Cycles instructionShiftRightAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 value = state.memory.read(address);
    const uint8 result = shiftRight(value, state.registers);
    state.memory.write(address, result);
    if (state.log)
    {
        state.log() << "srl" << toHex(address);
    }
    return 4;
}

Cycles instructionSwapLowAndHighRegister8(const ProgramState& state)
{
    const uint8 registerBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(registerBits);
    const uint8 value = state.registers.get(target);
    const uint8 result = swapLowAndHigh(value, state.registers);
    state.registers.set(target, result);
    if (state.log)
    {
        state.log() << "swap" << toString(target);
    }
    return 2;
}

Cycles instructionSwapLowAndHighAddressHl(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 value = state.memory.read(address);
    const uint8 result = swapLowAndHigh(value, state.registers);
    state.memory.write(address, result);
    if (state.log)
    {
        state.log() << "swap" << toHex(address);
    }
    return 4;
}

Cycles instructionGetBitInRegister(const ProgramState& state)
{
    const uint8 registerBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(registerBits);
    const uint8 value = state.registers.get(target);

    const uint8 bitIndex = getOffsetBits<3, 3>(state.instruction);
    state.registers.setZeroFlag(!isBitTrue(value, bitIndex));
    state.registers.setSubtractionFlag(false);
    state.registers.setCarryOnBit3(true);

    if (state.log)
    {
        state.log() << "bit" << toHex(bitIndex) << "in" << toString(target);
    }
    return 2;
}

Cycles instructionGetBitInAddress(const ProgramState& state)
{
    const uint16 address = state.registers.hl();
    const uint8 value = state.memory.read(address);

    const uint8 bitIndex = getOffsetBits<3, 3>(state.instruction);
    state.registers.setZeroFlag(!isBitTrue(value, bitIndex));
    state.registers.setSubtractionFlag(false);
    state.registers.setCarryOnBit3(true);

    if (state.log)
    {
        state.log() << "bit" << toHex(bitIndex) << "in" << toHex(address);
    }
    return 3;
}

Cycles instructionSetBitInRegister(const ProgramState& state)
{
    const uint8 highBits = getOffsetBits<2, 6>(state.instruction);
    const bool bitValue = (highBits == 0b11);

    const uint8 bitIndex = getOffsetBits<3, 3>(state.instruction);
    const uint8 registerBits = getOffsetBits<3, 0>(state.instruction);
    const Register target = bitsToRegister(registerBits);
    uint8 value = state.registers.get(target);
    setBit(value, bitIndex, bitValue);
    state.registers.set(target, value);

    if (state.log)
    {
        state.log() << "res" << int_cast<int32>(bitIndex) << "to" << bitValue << "in" << toString(target);
    }
    return 2;
}

Cycles instructionSetBitInAddress(const ProgramState& state)
{
    const uint8 highBits = getOffsetBits<2, 6>(state.instruction);
    const bool bitValue = (highBits == 0b11);

    const uint8 bitIndex = getOffsetBits<3, 3>(state.instruction);
    const uint16 address = state.registers.hl();
    uint8 value = state.memory.read(address);
    setBit(value, bitIndex, bitValue);
    state.memory.write(address, value);

    if (state.log)
    {
        state.log() << "res" << int_cast<int32>(bitIndex) << "to" << bitValue << "in" << toHex(address);
    }
    return 4;
}

Cycles instructionBitOperation(const ProgramState& state)
{
    static const std::array<std::function<Cycles(const ProgramState&)>, 256> operationTable = {
        // 0
        instructionRotateLeftRegister8,
        instructionRotateLeftRegister8,
        instructionRotateLeftRegister8,
        instructionRotateLeftRegister8,
        instructionRotateLeftRegister8,
        instructionRotateLeftRegister8,
        instructionRotateAddressHl,
        instructionRotateLeftRegister8,
        instructionRotateRightRegister8,
        instructionRotateRightRegister8,
        // 10
        instructionRotateRightRegister8,
        instructionRotateRightRegister8,
        instructionRotateRightRegister8,
        instructionRotateRightRegister8,
        instructionRotateRightAddressHl,
        instructionRotateRightRegister8,
        instructionRotateLeftWithCarryRegister8,
        instructionRotateLeftWithCarryRegister8,
        instructionRotateLeftWithCarryRegister8,
        instructionRotateLeftWithCarryRegister8,
        // 20
        instructionRotateLeftWithCarryRegister8,
        instructionRotateLeftWithCarryRegister8,
        instructionRotateLeftWithCarryAddressHl,
        instructionRotateLeftWithCarryRegister8,
        instructionRotateRightWithCarryRegister8,
        instructionRotateRightWithCarryRegister8,
        instructionRotateRightWithCarryRegister8,
        instructionRotateRightWithCarryRegister8,
        instructionRotateRightWithCarryRegister8,
        instructionRotateRightWithCarryRegister8,
        // 30
        instructionRotateRightWithCarryAddressHl,
        instructionRotateRightWithCarryRegister8,
        instructionShiftLeftRegister8,
        instructionShiftLeftRegister8,
        instructionShiftLeftRegister8,
        instructionShiftLeftRegister8,
        instructionShiftLeftRegister8,
        instructionShiftLeftRegister8,
        instructionShiftLeftAddressHl,
        instructionShiftLeftRegister8,
        // 40
        instructionShiftRightKeepBit7Register8,
        instructionShiftRightKeepBit7Register8,
        instructionShiftRightKeepBit7Register8,
        instructionShiftRightKeepBit7Register8,
        instructionShiftRightKeepBit7Register8,
        instructionShiftRightKeepBit7Register8,
        instructionShiftRightKeepBit7AddressHl,
        instructionShiftRightKeepBit7Register8,
        instructionSwapLowAndHighRegister8,
        instructionSwapLowAndHighRegister8,
        // 50
        instructionSwapLowAndHighRegister8,
        instructionSwapLowAndHighRegister8,
        instructionSwapLowAndHighRegister8,
        instructionSwapLowAndHighRegister8,
        instructionSwapLowAndHighAddressHl,
        instructionSwapLowAndHighRegister8,
        instructionShiftRightRegister8,
        instructionShiftRightRegister8,
        instructionShiftRightRegister8,
        instructionShiftRightRegister8,
        // 60
        instructionShiftRightRegister8,
        instructionShiftRightRegister8,
        instructionShiftRightAddressHl,
        instructionShiftRightRegister8,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        // 70
        instructionGetBitInAddress,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInAddress,
        instructionGetBitInRegister,
        // 80
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInAddress,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        // 90
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInAddress,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        // 100
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInAddress,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        // 110
        instructionGetBitInAddress,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInAddress,
        instructionGetBitInRegister,
        // 120
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInRegister,
        instructionGetBitInAddress,
        instructionGetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        // 130
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        // 140
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        // 150
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        // 160
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        // 170
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        // 180
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        // 190
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        // 200
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        // 210
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        // 220
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        // 230
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        // 240
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInAddress,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        // 250
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInRegister,
        instructionSetBitInAddress,
        instructionSetBitInRegister,
    };

    /**
     * Jump to the function that handles the bit operation.
     */
    const uint8 operation = takeIntegerFromPc(state.registers, state.memory);
    const ProgramState operationState{operation, state.registers, state.memory, state.log};
    const auto& function = operationTable[operation];
    return function(operationState);
}

} // namespace

Cycles execute(const uint8 instruction, Registers& registers, Memory& memory, Logger& log)
{
    static const std::array<std::function<Cycles(const ProgramState&)>, 256> instructionTable = {
        // 0
        instructionNoOperation,
        instructionLoadRegister16WithValue,
        instructionLoadAddressBcWithRegisterA,
        instructionIncrementRegister16,
        instructionIncrementRegister8,
        instructionDecrementRegister8,
        instructionLoadRegister8WithValue,
        instructionRotateLeftRegisterA,
        instructionLoadAddressPcWithRegisterSp,
        instructionAdditionRegister16WithRegisterHl,
        // 10
        instructionLoadRegisterAWithAddressBc,
        instructionDecrementRegister16,
        instructionIncrementRegister8,
        instructionDecrementRegister8,
        instructionLoadRegister8WithValue,
        instructionRotateRightRegisterA,
        instructionStop,
        instructionLoadRegister16WithValue,
        instructionLoadAddressDeWithRegisterA,
        instructionIncrementRegister16,
        // 20
        instructionIncrementRegister8,
        instructionDecrementRegister8,
        instructionLoadRegister8WithValue,
        instructionRotateLeftWithCarryRegisterA,
        instructionJumpPcOffsetAddress,
        instructionAdditionRegister16WithRegisterHl,
        instructionLoadRegisterAWithAddressDe,
        instructionDecrementRegister16,
        instructionIncrementRegister8,
        instructionDecrementRegister8,
        // 30
        instructionLoadRegister8WithValue,
        instructionRotateRightWithCarryRegisterA,
        instructionJumpPcOffsetAddressConditional,
        instructionLoadRegister16WithValue,
        instructionLoadAddressHlWithRegisterAIncrementHl,
        instructionIncrementRegister16,
        instructionIncrementRegister8,
        instructionDecrementRegister8,
        instructionLoadRegister8WithValue,
        instructionDecimalAdjustRegisterA,
        // 40
        instructionJumpPcOffsetAddressConditional,
        instructionAdditionRegister16WithRegisterHl,
        instructionLoadRegisterAWithAddressHlIncrementHl,
        instructionDecrementRegister16,
        instructionIncrementRegister8,
        instructionDecrementRegister8,
        instructionLoadRegister8WithValue,
        instructionNegateRegisterA,
        instructionJumpPcOffsetAddressConditional,
        instructionLoadRegister16WithValue,
        // 50
        instructionLoadAddressHlWithRegisterADecrementHl,
        instructionIncrementRegister16,
        instructionIncrementAddressHl,
        instructionDecrementAddressHl,
        instructionLoadAddressHlWithValue,
        instructionSetCarryFlag,
        instructionJumpPcOffsetAddressConditional,
        instructionAdditionRegister16WithRegisterHl,
        instructionLoadRegisterAWithAddressHlDecrementHl,
        instructionDecrementRegister16,
        // 60
        instructionIncrementRegister8,
        instructionDecrementRegister8,
        instructionLoadRegister8WithValue,
        instructionNegateCarryFlag,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        // 70
        instructionLoadRegister8WithAddressHl,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithAddressHl,
        instructionLoadRegister8WithRegister8,
        // 80
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithAddressHl,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        // 90
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithAddressHl,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        // 100
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithAddressHl,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        // 110
        instructionLoadRegister8WithAddressHl,
        instructionLoadRegister8WithRegister8,
        instructionLoadAddressHlWithRegister8,
        instructionLoadAddressHlWithRegister8,
        instructionLoadAddressHlWithRegister8,
        instructionLoadAddressHlWithRegister8,
        instructionLoadAddressHlWithRegister8,
        instructionLoadAddressHlWithRegister8,
        instructionHalt,
        instructionLoadAddressHlWithRegister8,
        // 120
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithRegister8,
        instructionLoadRegister8WithAddressHl,
        instructionLoadRegister8WithRegister8,
        instructionAdditionRegisterAWithRegister8,
        instructionAdditionRegisterAWithRegister8,
        // 130
        instructionAdditionRegisterAWithRegister8,
        instructionAdditionRegisterAWithRegister8,
        instructionAdditionRegisterAWithRegister8,
        instructionAdditionRegisterAWithRegister8,
        instructionAdditionRegisterAWithAddressHl,
        instructionAdditionRegisterAWithRegister8,
        instructionAdditionWithCarryRegisterAWithRegister8,
        instructionAdditionWithCarryRegisterAWithRegister8,
        instructionAdditionWithCarryRegisterAWithRegister8,
        instructionAdditionWithCarryRegisterAWithRegister8,
        // 140
        instructionAdditionWithCarryRegisterAWithRegister8,
        instructionAdditionWithCarryRegisterAWithRegister8,
        instructionAdditionWithCarryRegisterAWithAddressHl,
        instructionAdditionWithCarryRegisterAWithRegister8,
        instructionSubtractRegisterAWithRegister8,
        instructionSubtractRegisterAWithRegister8,
        instructionSubtractRegisterAWithRegister8,
        instructionSubtractRegisterAWithRegister8,
        instructionSubtractRegisterAWithRegister8,
        instructionSubtractRegisterAWithRegister8,
        // 150
        instructionSubtractRegisterAWithAddressHl,
        instructionSubtractRegisterAWithRegister8,
        instructionSubtractWithCarryRegisterAWithRegister8,
        instructionSubtractWithCarryRegisterAWithRegister8,
        instructionSubtractWithCarryRegisterAWithRegister8,
        instructionSubtractWithCarryRegisterAWithRegister8,
        instructionSubtractWithCarryRegisterAWithRegister8,
        instructionSubtractWithCarryRegisterAWithRegister8,
        instructionSubtractWithCarryRegisterAWithAddressHl,
        instructionSubtractWithCarryRegisterAWithRegister8,
        // 160
        instructionAndRegisterAWithRegister8,
        instructionAndRegisterAWithRegister8,
        instructionAndRegisterAWithRegister8,
        instructionAndRegisterAWithRegister8,
        instructionAndRegisterAWithRegister8,
        instructionAndRegisterAWithRegister8,
        instructionAndRegisterAWithAddressHl,
        instructionAndRegisterAWithRegister8,
        instructionXorRegisterAWithRegister8,
        instructionXorRegisterAWithRegister8,
        // 170
        instructionXorRegisterAWithRegister8,
        instructionXorRegisterAWithRegister8,
        instructionXorRegisterAWithRegister8,
        instructionXorRegisterAWithRegister8,
        instructionXorRegisterAWithAddressHl,
        instructionXorRegisterAWithRegister8,
        instructionOrRegisterAWithRegister8,
        instructionOrRegisterAWithRegister8,
        instructionOrRegisterAWithRegister8,
        instructionOrRegisterAWithRegister8,
        // 180
        instructionOrRegisterAWithRegister8,
        instructionOrRegisterAWithRegister8,
        instructionOrRegisterAWithAddressHl,
        instructionOrRegisterAWithRegister8,
        instructionCompareRegisterAWithRegister8,
        instructionCompareRegisterAWithRegister8,
        instructionCompareRegisterAWithRegister8,
        instructionCompareRegisterAWithRegister8,
        instructionCompareRegisterAWithRegister8,
        instructionCompareRegisterAWithRegister8,
        // 190
        instructionCompareRegisterAWithAddressHl,
        instructionCompareRegisterAWithRegister8,
        instructionReturnConditional,
        instructionPopRegister16,
        instructionJumpAddressPcConditional,
        instructionJumpAddressPc,
        instructionCallAddressPcConditional,
        instructionPushRegister16,
        instructionAdditionRegisterAWithValue,
        instructionRestart,
        // 200
        instructionReturnConditional,
        instructionReturn,
        instructionJumpAddressPcConditional,
        instructionBitOperation,
        instructionCallAddressPcConditional,
        instructionCallAddressPc,
        instructionAdditionWithCarryRegisterAWithValue,
        instructionRestart,
        instructionReturnConditional,
        instructionPopRegister16,
        // 210
        instructionJumpAddressPcConditional,
        instructionInvalid,
        instructionCallAddressPcConditional,
        instructionPushRegister16,
        instructionSubtractRegisterAWithValue,
        instructionRestart,
        instructionReturnConditional,
        instructionReturnEnableInterrupts,
        instructionJumpAddressPcConditional,
        instructionInvalid,
        // 220
        instructionCallAddressPcConditional,
        instructionInvalid,
        instructionSubtractWithCarryRegisterAWithValue,
        instructionRestart,
        instructionLoadPcOffsetAddressWithRegisterA,
        instructionPopRegister16,
        instructionLoadOffsetAddressCWithRegisterA,
        instructionInvalid,
        instructionInvalid,
        instructionPushRegister16,
        // 230
        instructionAndRegisterAWithValue,
        instructionRestart,
        instructionAdditionRegisterSpWithSignedPc,
        instructionJumpRegisterHl,
        instructionLoadAddressPcWithRegisterA,
        instructionInvalid,
        instructionInvalid,
        instructionInvalid,
        instructionXorRegisterAWithValue,
        instructionRestart,
        // 240
        instructionLoadRegisterAWithPcOffsetAddress,
        instructionPopRegister16,
        instructionLoadRegisterAWithOffsetAddressC,
        instructionDisableInterrupts,
        instructionInvalid,
        instructionPushRegister16,
        instructionOrRegisterAWithValue,
        instructionRestart,
        instructionLoadRegisterHlWithSpPlusPcOffset,
        instructionLoadRegisterSpWithRegisterHl,
        // 250
        instructionLoadRegisterAWithAddressPc,
        instructionEnableInterrupts,
        instructionInvalid,
        instructionInvalid,
        instructionCompareRegisterAWithValue,
        instructionRestart,
    };

    /**
     * Jump to the function that handles the instruction.
     */
    const ProgramState state{instruction, registers, memory, log};
    const auto& function = instructionTable[instruction];
    return function(state);
}

} // namespace gb
