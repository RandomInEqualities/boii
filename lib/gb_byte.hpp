#pragma once

#include "gb_int.hpp"

#include <cstdint>
#include <filesystem>
#include <vector>

namespace gb
{

/**
 * A vector to denote an array of bytes.
 */
using ByteVector = std::vector<std::uint8_t>;

/**
 * Read all bytes in the given file.
 *
 * Throws IO exceptions if anything goes wrong.
 */
ByteVector readBytesFromFile(const std::filesystem::path& path);

/**
 * Write the given bytes into the given file.
 *
 * Deletes any existing bytes in the file.
 */
void writeBytesToFile(const std::filesystem::path& path, const ByteVector& bytes);

/**
 * Create an array and fill it with the given amount of random bytes.
 */
ByteVector createRandomBytes(int64 amount);

} // namespace gb
