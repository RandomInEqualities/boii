#pragma once

#include "gb_assert.hpp"
#include "gb_int.hpp"

namespace gb
{

/**
 * Class to count cycles.
 *
 * Resets to cycle 0 when reset cycle is reached.
 */
class CycleCounter
{
public:
    explicit CycleCounter(const Cycles resetCycle)
    {
        setResetCycle(resetCycle);
    }

    void setEnabled(const bool enabled)
    {
        m_enabled = enabled;
    }

    void setResetCycle(const Cycles resetCycle)
    {
        GB_ASSERT(resetCycle > 0);
        m_resetCycle = resetCycle;
        m_cycles = 0;
    }

    bool step(const Cycles cycles)
    {
        GB_ASSERT(cycles > 0);
        if (m_enabled)
        {
            m_cycles += cycles;
            if (m_cycles >= m_resetCycle)
            {
                m_cycles = (m_cycles - m_resetCycle);
                return true;
            }
        }
        return false;
    }

private:
    bool m_enabled = true;
    Cycles m_cycles = 0;
    Cycles m_resetCycle = 0;
};

} // namespace gb
