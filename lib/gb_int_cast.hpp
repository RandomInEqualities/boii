#pragma once

#include "gb_assert.hpp"

#include <concepts>
#include <limits>
#include <type_traits>

namespace gb
{

/**
 * Convert an integer type to another integer type.
 *
 * Throws AssertionException if value is not representable in the target integer.
 */
template<std::integral Target, std::integral Source>
[[nodiscard]] constexpr Target int_cast(const Source value)
{
    if constexpr (std::is_signed_v<Target> && std::is_signed_v<Source>)
    {
        GB_ASSERT(value >= std::numeric_limits<Target>::min());
        GB_ASSERT(value <= std::numeric_limits<Target>::max());
    }
    if constexpr (std::is_signed_v<Target> && !std::is_signed_v<Source>)
    {
        using TargetAsUnsigned [[maybe_unused]] = std::make_unsigned_t<Target>;
        GB_ASSERT(value <= static_cast<TargetAsUnsigned>(std::numeric_limits<Target>::max()));
    }
    if constexpr (!std::is_signed_v<Target> && std::is_signed_v<Source>)
    {
        GB_ASSERT(value >= 0);
        using SourceAsUnsigned [[maybe_unused]] = std::make_unsigned_t<Source>;
        GB_ASSERT(static_cast<SourceAsUnsigned>(value) <= std::numeric_limits<Target>::max());
    }
    if constexpr (!std::is_signed_v<Target> && !std::is_signed_v<Source>)
    {
        GB_ASSERT(value <= std::numeric_limits<Target>::max());
    }

    return static_cast<Target>(value);
}

} // namespace gb
