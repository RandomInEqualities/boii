#pragma once

#include "gb_byte.hpp"
#include "gb_cartridge.hpp"
#include "gb_registers.hpp"
#include "gb_systems.hpp"

namespace gb
{

/**
 * Controller for the blocks of memory in the Game Boy.
 *
 * Will delegate writes and reads to the cartridge or other systems
 * if addresses are for them.
 */
class Memory
{
public:
    explicit Memory(Registers&, Systems&, Cartridge&);

    /**
     * Write a byte at the specified address.
     */
    void write(Address, Byte);

    /**
     * Read a byte at the specified address.
     */
    Byte read(Address) const;

private:
    void dmaTransfer(uint8 request);

    /**
     * Low and High RAM segments.
     */
    ByteVector m_lowRam;
    ByteVector m_highRam;

    /**
     * Controllers for other systems.
     */
    Registers& m_registers;
    Systems& m_systems;
    Cartridge& m_cartridge;
};

} // namespace gb
