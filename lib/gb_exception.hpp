#pragma once

#include <source_location>
#include <stdexcept>
#include <string>

namespace gb
{

/**
 * Base exception class for all exceptions.
 */
class Exception : public std::runtime_error
{
public:
    explicit Exception(const std::string& message);
};

/**
 * Thrown when an assertion error occur.
 */
class AssertException final : public Exception
{
public:
    explicit AssertException(const std::string& condition,
                             const std::source_location& location = std::source_location::current());
};

/**
 * Thrown when something cannot possibly happen.
 */
class InternalErrorException final : public Exception
{
public:
    explicit InternalErrorException();
};

/**
 * Thrown when something is not supported.
 */
class UnsupportedException final : public Exception
{
public:
    using Exception::Exception;
};

/**
 * Thrown when something is not implement, yet.
 */
class UnimplementedException final : public Exception
{
public:
    using Exception::Exception;
};

/**
 * Thrown when an io error happens. E.g. we could not save a file.
 */
class IoException final : public Exception
{
public:
    using Exception::Exception;
};

} // namespace gb
