#pragma once

#include <functional>
#include <ostream>
#include <utility>
#include <vector>

namespace gb
{

/**
 * Helper to log information to one or more ostream's.
 *
 * It can be used like this:
 *
 * Logger log({std::cout});
 * log() << "Hello" << "World";
 *
 * Space is automatically added between << operators and a newline is added at the end of expressions.
 */
class Logger
{
public:
    using Streams = std::vector<std::reference_wrapper<std::ostream>>;

    explicit Logger(Streams streams = {})
        : m_streams(std::move(streams))
    {
    }

    ~Logger() noexcept(false)
    {
        for (const auto& stream : m_streams)
        {
            stream.get() << "\n";
        }
    }

    template<class T>
    Logger& operator<<(const T& value)
    {
        for (const auto& stream : m_streams)
        {
            stream.get() << value << " ";
        }
        return *this;
    }

    void flush()
    {
        for (const auto& stream : m_streams)
        {
            stream.get() << std::flush;
        }
    }

    explicit operator bool() const
    {
        return !m_streams.empty();
    }

    Logger operator()()
    {
        return *this;
    }

    Logger(const Logger&) = default;
    Logger& operator=(const Logger&) = default;
    Logger(Logger&&) = default;
    Logger& operator=(Logger&&) = default;

private:
    Streams m_streams;
};

/**
 * Default logger that does not log anything.
 */
static inline Logger nullLog;

} // namespace gb
