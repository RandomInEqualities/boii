#include "gb_byte.hpp"
#include "gb_int_cast.hpp"

#include <algorithm>
#include <fstream>
#include <random>

namespace gb
{

ByteVector readBytesFromFile(const std::filesystem::path& path)
{
    std::ifstream file;
    file.exceptions(std::ios::failbit | std::ios::badbit | std::ios::eofbit);
    file.open(path, std::ios::in | std::ios::binary | std::ios::ate);
    const std::streamoff length = file.tellg();
    file.seekg(0, std::ios::beg);
    ByteVector bytes(int_cast<ByteVector::size_type>(length));
    file.read(reinterpret_cast<char*>(bytes.data()), length);
    return bytes;
}

void writeBytesToFile(const std::filesystem::path& path, const ByteVector& bytes)
{
    std::ofstream file;
    file.exceptions(std::ios::failbit | std::ios::badbit | std::ios::eofbit);
    file.open(path, std::ios::out | std::ios::binary | std::ios::trunc);
    const auto length = int_cast<std::streamsize>(bytes.size());
    file.write(reinterpret_cast<const char*>(bytes.data()), length);
}

ByteVector createRandomBytes(const int64 amount)
{
    std::random_device seed_device;
    std::mt19937 generator(seed_device());
    std::uniform_int_distribution<uint16> distribution(0, 255);
    ByteVector data(int_cast<ByteVector::size_type>(amount));
    std::ranges::generate(data, [&] { return int_cast<Byte>(distribution(generator)); });
    return data;
}

} // namespace gb
