#pragma once

#include "gb_int.hpp"
#include "gb_logger.hpp"
#include "gb_memory.hpp"
#include "gb_registers.hpp"

namespace gb
{

/**
 * Execute the given instruction modifying the passed in registers and memory.
 *
 * Returns the amount of cycles that it took to execute the instruction.
 */
Cycles execute(uint8 instruction, Registers& registers, Memory& memory, Logger& log);

} // namespace gb
