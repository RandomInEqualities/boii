#pragma once

#include "gb_assert.hpp"
#include "gb_int.hpp"
#include "gb_int_cast.hpp"

#include <concepts>

namespace gb
{

/**
 * Get the low part of a uint16 (the 0x00XX part).
 */
constexpr uint8 getLow(const uint16 target)
{
    return target & 0x00FF;
}

/**
 * Set the low part of a uint16 (the 0x00XX part).
 */
constexpr void setLow(uint16& target, const uint8 low)
{
    target |= 0x00FF;

    const uint16 low16 = int_cast<uint16>(low) | 0xFF00;
    target &= low16;
}

/**
 * Get the high part of a uint16 (the 0xXX00 part).
 */
constexpr uint8 getHigh(const uint16 target)
{
    return (target >> 8) & 0x00FF;
}

/**
 * Set the high part of a uint16 (the 0xXX00 part).
 */
constexpr void setHigh(uint16& target, const uint8 high)
{
    target |= 0xFF00;

    const uint16 high16 = int_cast<uint16>(high << 8) | 0x00FF;
    target &= high16;
}

/**
 * Check the value of a bit (zero-indexed, the least significant bit is at 0).
 */
template<std::integral Integer>
constexpr bool isBitTrue(const Integer target, const uint8 index)
{
    GB_ASSERT(index <= 8 * sizeof(Integer));
    const int32 blitter = (0x0001 << index);
    return (target & blitter) != 0;
}

/**
 * Set the value of a bit (zero-indexed, the least significant bit is at 0).
 */
template<std::integral Integer>
constexpr void setBit(Integer& target, const uint8 index, const bool value)
{
    GB_ASSERT(index <= 8 * sizeof(Integer));
    if (value)
    {
        const int32 blitter = (0x0001 << index);
        target |= blitter;
    }
    else
    {
        const int32 blitter = ~(0x0001 << index);
        target &= blitter;
    }
}

/**
 * Get a specific amount of bits set to 1 in a uint8.
 *
 * E.g. amount==1 returns 0b1, amount==2 returns 0b11, etc.
 */
template<uint8 Amount>
constexpr uint8 getOneBits()
{
    static_assert(1 <= Amount && Amount <= 8);
    constexpr uint8 number = 0b11111111;
    return number >> (8 - Amount);
}

/**
 * Get an amount of bits in the given number at the specified offset.
 *
 * For example:
 *      getOffsetBits<3, 0>(0b00000101) == 0b101.
 *      getOffsetBits<3, 3>(0b00101000) == 0b101.
 *      getOffsetBits<3, 5>(0b10100000) == 0b101.
 */
template<uint8 Amount, uint8 Offset>
constexpr uint8 getOffsetBits(const uint8 number)
{
    static_assert(Offset <= 7);
    static_assert(Amount <= 8);
    return (number >> Offset) & getOneBits<Amount>();
}

} // namespace gb
