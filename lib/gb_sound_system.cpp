#include "gb_sound_system.hpp"
#include "gb_exception.hpp"

namespace gb
{

void SoundSystem::write(const Address address, const Byte value)
{
    if (address == 0xFF10)
    {
        m_mode1_ff10 = value;
    }
    else if (address == 0xFF11)
    {
        m_mode1_ff11 = value;
    }
    else if (address == 0xFF12)
    {
        m_mode1_ff12 = value;
    }
    else if (address == 0xFF13)
    {
        m_mode1_ff13 = value;
    }
    else if (address == 0xFF14)
    {
        m_mode1_ff14 = value;
    }
    else if (address == 0xFF16)
    {
        m_mode2_ff16 = value;
    }
    else if (address == 0xFF17)
    {
        m_mode2_ff17 = value;
    }
    else if (address == 0xFF18)
    {
        m_mode2_ff18 = value;
    }
    else if (address == 0xFF19)
    {
        m_mode2_ff19 = value;
    }
    else if (address == 0xFF1A)
    {
        m_mode3_ff1a = value;
    }
    else if (address == 0xFF1B)
    {
        m_mode3_ff1b = value;
    }
    else if (address == 0xFF1C)
    {
        m_mode3_ff1c = value;
    }
    else if (address == 0xFF1D)
    {
        m_mode3_ff1d = value;
    }
    else if (address == 0xFF1E)
    {
        m_mode3_ff1e = value;
    }
    else if (address == 0xFF20)
    {
        m_mode4_ff20 = value;
    }
    else if (address == 0xFF21)
    {
        m_mode4_ff21 = value;
    }
    else if (address == 0xFF22)
    {
        m_mode4_ff22 = value;
    }
    else if (address == 0xFF23)
    {
        m_mode4_ff23 = value;
    }
    else if (address == 0xFF24)
    {
        m_control_ff24 = value;
    }
    else if (address == 0xFF25)
    {
        m_control_ff25 = value;
    }
    else if (address == 0xFF26)
    {
        m_control_ff26 = value;
    }
    else if (address >= 0xFF30 && address < 0xFF40)
    {
        m_mode3_waveform.at(address - 0xFF30) = value;
    }
    else
    {
        throw UnimplementedException("unknown sound port");
    }
}

Byte SoundSystem::read(const Address address) const
{
    if (address == 0xFF10)
    {
        return m_mode1_ff10;
    }
    else if (address == 0xFF11)
    {
        return m_mode1_ff11;
    }
    else if (address == 0xFF12)
    {
        return m_mode1_ff12;
    }
    else if (address == 0xFF13)
    {
        return m_mode1_ff13;
    }
    else if (address == 0xFF14)
    {
        return m_mode1_ff14;
    }
    else if (address == 0xFF16)
    {
        return m_mode2_ff16;
    }
    else if (address == 0xFF17)
    {
        return m_mode2_ff17;
    }
    else if (address == 0xFF18)
    {
        return m_mode2_ff18;
    }
    else if (address == 0xFF19)
    {
        return m_mode2_ff19;
    }
    else if (address == 0xFF1A)
    {
        return m_mode3_ff1a;
    }
    else if (address == 0xFF1B)
    {
        return m_mode3_ff1b;
    }
    else if (address == 0xFF1C)
    {
        return m_mode3_ff1c;
    }
    else if (address == 0xFF1D)
    {
        return m_mode3_ff1d;
    }
    else if (address == 0xFF1E)
    {
        return m_mode3_ff1e;
    }
    else if (address == 0xFF20)
    {
        return m_mode4_ff20;
    }
    else if (address == 0xFF21)
    {
        return m_mode4_ff21;
    }
    else if (address == 0xFF22)
    {
        return m_mode4_ff22;
    }
    else if (address == 0xFF23)
    {
        return m_mode4_ff23;
    }
    else if (address == 0xFF24)
    {
        return m_control_ff24;
    }
    else if (address == 0xFF25)
    {
        return m_control_ff25;
    }
    else if (address == 0xFF26)
    {
        return m_control_ff26;
    }
    else if (address >= 0xFF30 && address < 0xFF40)
    {
        return m_mode3_waveform.at(address - 0xFF30);
    }
    else
    {
        throw UnimplementedException("unknown sound port");
    }
}

} // namespace gb
