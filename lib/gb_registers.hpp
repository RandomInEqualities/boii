#pragma once

#include "gb_bit_utilities.hpp"
#include "gb_exception.hpp"

#include <string_view>

namespace gb
{

/**
 * The 8 8-bit registers in the Game Boy.
 */
enum class Register : uint8
{
    A,
    F,
    B,
    C,
    D,
    E,
    H,
    L,
};

/**
 * The 6 16-bit registers in the Game Boy.
 */
enum class RegisterPair : uint8
{
    /**
     * The 8 8-bit registers grouped into 16-bit registers.
     */
    AF,
    BC,
    DE,
    HL,
    /**
     * The register that holds the stack pointer.
     */
    SP,
    /**
     * The register that holds the program counter.
     */
    PC,
};

/**
 * Class to manage the registers of the Game Boy.
 */
class Registers
{
public:
    /**
     * Get the value of the given register.
     */
    constexpr uint8 get(const Register id) const
    {
        switch (id)
        {
        case Register::A:
            return getHigh(m_af);
        case Register::F:
            return getLow(m_af);
        case Register::B:
            return getHigh(m_bc);
        case Register::C:
            return getLow(m_bc);
        case Register::D:
            return getHigh(m_de);
        case Register::E:
            return getLow(m_de);
        case Register::H:
            return getHigh(m_hl);
        case Register::L:
            return getLow(m_hl);
        }
        throw InternalErrorException();
    }

    /**
     * Set the value of the given register.
     */
    constexpr void set(const Register id, const uint8 value)
    {
        switch (id)
        {
        case Register::A:
            setHigh(m_af, value);
            break;
        case Register::F:
            setLow(m_af, value);
            sanitizeFlags();
            break;
        case Register::B:
            setHigh(m_bc, value);
            break;
        case Register::C:
            setLow(m_bc, value);
            break;
        case Register::D:
            setHigh(m_de, value);
            break;
        case Register::E:
            setLow(m_de, value);
            break;
        case Register::H:
            setHigh(m_hl, value);
            break;
        case Register::L:
            setLow(m_hl, value);
            break;
        }
    }

    /**
     * Get the value of the given register.
     */
    constexpr uint16 get(const RegisterPair pair) const
    {
        switch (pair)
        {
        case RegisterPair::AF:
            return m_af;
        case RegisterPair::BC:
            return m_bc;
        case RegisterPair::DE:
            return m_de;
        case RegisterPair::HL:
            return m_hl;
        case RegisterPair::SP:
            return m_sp;
        case RegisterPair::PC:
            return m_pc;
        }
        throw InternalErrorException();
    }

    /**
     * Set the value of the given register.
     */
    constexpr void set(const RegisterPair pair, const uint16 value)
    {
        switch (pair)
        {
        case RegisterPair::AF:
            m_af = value;
            sanitizeFlags();
            break;
        case RegisterPair::BC:
            m_bc = value;
            break;
        case RegisterPair::DE:
            m_de = value;
            break;
        case RegisterPair::HL:
            m_hl = value;
            break;
        case RegisterPair::SP:
            m_sp = value;
            break;
        case RegisterPair::PC:
            m_pc = value;
            break;
        }
    }

    /**
     * Set or get the 16-bit hl register.
     */
    constexpr uint16 hl() const
    {
        return m_hl;
    }
    constexpr void setHl(const uint16 hl)
    {
        m_hl = hl;
    }

    /**
     * Set or get the current stack address.
     */
    constexpr uint16 sp() const
    {
        return m_sp;
    }
    constexpr void setSp(const uint16 sp)
    {
        m_sp = sp;
    }

    /**
     * Set or get the current program address.
     */
    constexpr uint16 pc() const
    {
        return m_pc;
    }
    constexpr void setPc(const uint16 pc)
    {
        m_pc = pc;
    }

    /**
     * Flag set then addition or subtraction results in zero. Also called the Z flag.
     */
    constexpr bool haveZeroFlag() const
    {
        return isBitTrue(m_af, 7);
    }

    /**
     * Is in general set to true when addition/subtraction result in zero.
     */
    constexpr void setZeroFlag(const bool value)
    {
        setBit(m_af, 7, value);
    }

    /**
     * Flag set when subtracting numbers. Also called the N flag.
     */
    constexpr bool haveSubtractionFlag() const
    {
        return isBitTrue(m_af, 6);
    }

    /**
     * Is in general set to true when subtracting, false otherwise.
     */
    constexpr void setSubtractionFlag(const bool value)
    {
        setBit(m_af, 6, value);
    }

    /**
     * Flag set when addition or subtraction overflows from bit 4. Also called the H flag.
     */
    constexpr bool haveCarryOnBit3() const
    {
        return isBitTrue(m_af, 5);
    }

    /**
     * Is in general set to true when lower bits (0xF) in addition or subtraction results in carry to/from bit 4.
     */
    constexpr void setCarryOnBit3(const bool value)
    {
        setBit(m_af, 5, value);
    }

    /**
     * Flag set then addition or subtraction overflows from bit 8. Also called the C (or CY) flag.
     */
    constexpr bool haveCarryOnBit7() const
    {
        return isBitTrue(m_af, 4);
    }

    /**
     * Is in general set to true when addition or subtraction results in overflow.
     */
    constexpr void setCarryOnBit7(const bool value)
    {
        setBit(m_af, 4, value);
    }

    /**
     * Reset all flags to zero.
     */
    constexpr void resetFlags()
    {
        setZeroFlag(false);
        setCarryOnBit3(false);
        setCarryOnBit7(false);
        setSubtractionFlag(false);
    }

    /**
     * Get or set if interrupts are enabled or disabled. Also called the IME register.
     */
    constexpr bool isInterruptsEnabled() const
    {
        return m_isInterruptsEnabled;
    }
    constexpr void setInterruptsEnabled(const bool enabled)
    {
        m_isInterruptsEnabled = enabled;
    }

    /**
     * Get or set the mask for the interrupts that have been signalled (triggered). Also called the IF register.
     */
    constexpr Byte getSignalledInterrupts() const
    {
        return m_signalledInterrupts;
    }
    constexpr void setSignalledInterrupts(const Byte interrupts)
    {
        m_signalledInterrupts = interrupts & 0b00011111;
    }

    /**
     * Get or set the mask for the interrupts that are enabled. Also called the IE register.
     */
    constexpr Byte getEnabledInterrupts() const
    {
        return m_enabledInterrupts;
    }
    constexpr void setEnabledInterrupts(const Byte interrupts)
    {
        m_enabledInterrupts = interrupts & 0b00011111;
    }

    /**
     * Check if any interrupt has been signalled and is enabled.
     */
    constexpr bool haveAnyInterrupt() const
    {
        return (m_enabledInterrupts & m_signalledInterrupts) != 0;
    }

    /**
     * Check if the interrupt at the given index is enabled and signalled.
     */
    constexpr bool haveInterruptAtIndex(const uint8 index) const
    {
        return isBitTrue(m_enabledInterrupts, index) && isBitTrue(m_signalledInterrupts, index);
    }

    /**
     * Set or reset the interrupt at the given index as signalled.
     */
    constexpr void setInterruptAtIndex(const uint8 index)
    {
        setBit(m_signalledInterrupts, index, true);
    }
    constexpr void resetInterruptAtIndex(const uint8 index)
    {
        setBit(m_signalledInterrupts, index, false);
    }

    /**
     * Set or get if the CPU is stopped, such that it won't execute instructions until next key input.
     */
    constexpr bool isProcessorStopped() const
    {
        return m_isProcessorStopped;
    }
    constexpr void setProcessorStopped(const bool stopped)
    {
        m_isProcessorStopped = stopped;
    }

    /**
     * Set or get if the CPU is halted waiting for an interrupt to wake it, before a jump to the halt address.
     */
    constexpr bool isProcessorHalted() const
    {
        return m_processorHalt.has_value();
    }
    constexpr Address getProcessorHalt() const
    {
        return m_processorHalt.value();
    }
    constexpr void setProcessorHalt(const Address haltAddress)
    {
        m_processorHalt = haltAddress;
    }
    constexpr void clearProcessorHalt()
    {
        m_processorHalt.reset();
    }

private:
    constexpr void sanitizeFlags()
    {
        /**
         * Lowest 4 bits of the flag register should always be zero.
         */
        m_af = m_af & 0xFFF0;
    }

    /**
     * Default values from mGBA.
     */
    uint16 m_af = 0x01B0;
    uint16 m_bc = 0x0013;
    uint16 m_de = 0x00D8;
    uint16 m_hl = 0x014D;
    uint16 m_sp = 0xFFFE;
    uint16 m_pc = 0x0100;

    /**
     * IME: If interrupts are enabled or disabled.
     */
    bool m_isInterruptsEnabled = false;

    /**
     * IF: The interrupts that are signalled.
     */
    Byte m_signalledInterrupts = 0;

    /**
     * IE: The interrupts that are enabled.
     */
    Byte m_enabledInterrupts = 0;

    /**
     * If the CPU is stopped so it won't execute instructions until next key input.
     */
    bool m_isProcessorStopped = false;

    /**
     * The CPU halt address. Puts the CPU to sleep waiting for an interrupt.
     */
    std::optional<Address> m_processorHalt;
};

constexpr std::string_view toString(const Register id)
{
    switch (id)
    {
    case Register::A:
        return "A";
    case Register::F:
        return "F";
    case Register::B:
        return "B";
    case Register::C:
        return "C";
    case Register::D:
        return "D";
    case Register::E:
        return "E";
    case Register::H:
        return "H";
    case Register::L:
        return "L";
    }
    throw InternalErrorException();
}

constexpr std::string_view toString(const RegisterPair id)
{
    switch (id)
    {
    case RegisterPair::AF:
        return "AF";
    case RegisterPair::BC:
        return "BC";
    case RegisterPair::DE:
        return "DE";
    case RegisterPair::HL:
        return "HL";
    case RegisterPair::SP:
        return "SP";
    case RegisterPair::PC:
        return "PC";
    }
    throw InternalErrorException();
}

} // namespace gb
