#include "gb_serial_system.hpp"
#include "gb_bit_utilities.hpp"
#include "gb_exception.hpp"

namespace gb
{

void SerialSystem::write(const Address address, const Byte value)
{
    if (address == 0xFF01)
    {
        m_transferData = value;
    }
    else if (address == 0xFF02)
    {
        m_useInternalClock = isBitTrue(value, 0);
        m_hasTransfer = isBitTrue(value, 7);
    }
    else
    {
        throw InternalErrorException();
    }
}

Byte SerialSystem::read(const Address address) const
{
    if (address == 0xFF01)
    {
        return m_transferData;
    }
    else if (address == 0xFF02)
    {
        uint8 result = 0xFF;
        setBit(result, 0, m_useInternalClock);
        setBit(result, 7, m_hasTransfer);
        return result;
    }
    throw InternalErrorException();
}

bool SerialSystem::interrupt(const Cycles)
{
    if (m_hasTransfer && m_useInternalClock)
    {
        if (m_transferNotifier)
        {
            m_transferNotifier(m_transferData);
        }

        /**
         * 0xFF notifies the game that things have been read.
         */
        m_transferData = 0xFF;
        m_hasTransfer = false;
        return true;
    }
    return false;
}

void SerialSystem::setTransferNotifier(const TransferNotifier& transferNotifier)
{
    m_transferNotifier = transferNotifier;
}

} // namespace gb
