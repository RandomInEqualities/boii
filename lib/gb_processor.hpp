#pragma once

#include "gb_cartridge.hpp"
#include "gb_input_system.hpp"
#include "gb_int.hpp"
#include "gb_lcd_system.hpp"
#include "gb_logger.hpp"
#include "gb_memory.hpp"
#include "gb_registers.hpp"
#include "gb_serial_system.hpp"
#include "gb_sound_system.hpp"
#include "gb_systems.hpp"

namespace gb
{

/**
 * CPU emulator for the original Game Boy machine.
 *
 * Game Boy chip name: SHARP LR35902
 * Game Boy runs at 4MHz (4194304Hz) clock cycles.
 */
class Processor
{
public:
    explicit Processor(Cartridge&);

    /**
     * Access to various Game Boy systems.
     */
    InputSystem& input();
    LcdSystem& lcd();
    SoundSystem& sound();
    SerialSystem& serial();

    /**
     * Execute next instruction in the game.
     */
    Cycles step(Logger& log);

private:
    /**
     * Execute the next instruction with the current program state.
     */
    Cycles executeNextInstruction(Logger& log);
    uint8 takeInstruction();
    void jumpTo(Address);

    /**
     * Process interrupts and find the potential interrupt jump address.
     */
    void propagateCycles(Cycles);
    std::optional<Address> checkForInterrupt();
    Address processNormalInterrupt();
    Address processHaltInterrupt();

    Cartridge& m_cartridge;
    Systems m_systems;
    Registers m_registers;
    Memory m_memory;
};

} // namespace gb
