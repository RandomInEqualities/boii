#pragma once

#include "gb_int.hpp"

#include <array>

namespace gb
{

/**
 * Sound system to handle playing Game Boy sounds.
 *
 * Decoding of the sound registers is not implemented for now.
 */
class SoundSystem
{
public:
    /**
     * Write the given byte at the specified address.
     */
    void write(Address, Byte);

    /**
     * Read the byte at the specified address.
     */
    Byte read(Address) const;

private:
    uint8 m_mode1_ff10 = 0;
    uint8 m_mode1_ff11 = 0;
    uint8 m_mode1_ff12 = 0;
    uint8 m_mode1_ff13 = 0;
    uint8 m_mode1_ff14 = 0;

    uint8 m_mode2_ff16 = 0;
    uint8 m_mode2_ff17 = 0;
    uint8 m_mode2_ff18 = 0;
    uint8 m_mode2_ff19 = 0;

    uint8 m_mode3_ff1a = 0;
    uint8 m_mode3_ff1b = 0;
    uint8 m_mode3_ff1c = 0;
    uint8 m_mode3_ff1d = 0;
    uint8 m_mode3_ff1e = 0;
    std::array<uint8, 16> m_mode3_waveform = {};

    uint8 m_mode4_ff20 = 0;
    uint8 m_mode4_ff21 = 0;
    uint8 m_mode4_ff22 = 0;
    uint8 m_mode4_ff23 = 0;

    uint8 m_control_ff24 = 0;
    uint8 m_control_ff25 = 0;
    uint8 m_control_ff26 = 0;
};

} // namespace gb
