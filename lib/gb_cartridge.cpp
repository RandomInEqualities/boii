#include "gb_cartridge.hpp"
#include "gb_bit_utilities.hpp"
#include "gb_exception.hpp"
#include "gb_int_cast.hpp"

namespace gb
{
namespace
{

enum class CartridgeType : uint8
{
    WithoutBank,
    MBC1,
    MBC2,
    MBC3,
    MBC5,
    Unknown,
};

CartridgeType getCartridgeType(const ByteVector& rom)
{
    switch (rom.at(0x147))
    {
    case 0x00:
        return CartridgeType::WithoutBank; // ROM
    case 0x01:
        return CartridgeType::MBC1; // MBC1
    case 0x02:
        return CartridgeType::MBC1; // MBC1+RAM
    case 0x03:
        return CartridgeType::MBC1; // MBC1+RAM+BATTERY
    case 0x05:
        return CartridgeType::MBC2; // MBC2
    case 0x06:
        return CartridgeType::MBC2; // MBC2+BATTERY
    case 0x08:
        return CartridgeType::WithoutBank; // ROM+RAM
    case 0x09:
        return CartridgeType::WithoutBank; // ROM+RAM+BATTERY
    case 0x11:
        return CartridgeType::MBC3; // MBC3
    case 0x12:
        return CartridgeType::MBC3; // MBC3+RAM
    case 0x13:
        return CartridgeType::MBC3; // MBC3+RAM+BATTERY
    case 0x19:
        return CartridgeType::MBC5; // MBC5
    case 0x1A:
        return CartridgeType::MBC5; // MBC5+RAM
    case 0x1B:
        return CartridgeType::MBC5; // MBC5+RAM+BATTERY
    case 0x1C:
        return CartridgeType::MBC5; // MBC5+RUMBLE
    case 0x1D:
        return CartridgeType::MBC5; // MBC5+RUMBLE+RAM
    case 0x1E:
        return CartridgeType::MBC5; // MBC5+RUMBLE+RAM+BATTERY
    default:
        return CartridgeType::Unknown;
    }
}

uint8 getRomBankCount(const ByteVector& rom)
{
    switch (rom.at(0x148))
    {
    case 0x00:
        return 2; // 32 KB rom
    case 0x01:
        return 4; // 64 KB rom
    case 0x02:
        return 8; // 128 KB rom
    case 0x03:
        return 16; // 256 KB rom
    case 0x04:
        return 32; // 512 KB rom
    case 0x05:
        return 64; // 1 MB rom
    case 0x06:
        return 128; // 2 MB rom
    }
    throw UnsupportedException("unsupported ROM bank count");
}

uint8 getRamBankCount(const ByteVector& rom)
{
    switch (rom.at(0x149))
    {
    case 0x00:
        return 0; // no ram
    case 0x02:
        return 1; // 8 KB ram
    case 0x03:
        return 4; // 32 KB ram
    case 0x05:
        return 8; // 64 KB ram
    }
    throw UnsupportedException("unsupported RAM bank count");
}

std::string getTitle(const ByteVector& rom)
{
    std::string title;
    for (std::size_t index = 0x134; index <= 0x142; index++)
    {
        const uint8 character = rom.at(index);
        if (character == 0)
        {
            break;
        }
        title.push_back(int_cast<char>(character));
    }
    return title;
}

class MemoryBankControllerWithoutBank final : public MemoryBankController
{
public:
    explicit MemoryBankControllerWithoutBank(const ByteVector& rom)
        : m_rom(rom)
    {
        if (rom.size() != 0x8000)
        {
            throw IoException("rom only cartridge must be 32 KB");
        }
        const uint8 ramBankCount = getRamBankCount(rom);
        if (ramBankCount != 0 && ramBankCount != 1)
        {
            throw IoException("rom only cartridge ram must be 0 or 8 KB");
        }
        m_ram = createRandomBytes(0x2000 * ramBankCount);
    }

    std::string title() const override
    {
        return getTitle(m_rom);
    }

    const ByteVector& ram() const override
    {
        return m_ram;
    }

    void setRam(const ByteVector& ram) override
    {
        if (m_ram.size() != ram.size())
        {
            throw IoException("saved cartridge ram size is not correct");
        }
        m_ram = ram;
    }

    void dump(Logger& log) const override
    {
        log() << "ROM:" << 1 << "RAM:" << 0;
    }

    void write(const Address address, const Byte value) override
    {
        if (address < 0x8000)
        {
            // Writes to this are ignored.
        }
        else if (address >= 0xA000 && address < 0xC000)
        {
            const uint16 bankAddress = (address - 0xA000);
            m_ram.at(bankAddress) = value;
        }
        else
        {
            throw InternalErrorException();
        }
    }

    Byte read(const Address address) const override
    {
        if (address < 0x8000)
        {
            return m_rom.at(address);
        }
        else if (address >= 0xA000 && address < 0xC000)
        {
            const uint16 bankAddress = (address - 0xA000);
            return m_ram.at(bankAddress);
        }
        throw InternalErrorException();
    }

private:
    ByteVector m_rom;
    ByteVector m_ram;
};

class MemoryBankController1 final : public MemoryBankController
{
public:
    explicit MemoryBankController1(const ByteVector& rom)
        : m_rom(rom)
    {
        const uint8 romBankCount = getRomBankCount(rom);
        if (rom.size() != (0x4000 * romBankCount))
        {
            throw IoException("cartridge rom have incorrect size");
        }
        m_ramBankCount = getRamBankCount(rom);
        m_ram = createRandomBytes(0x2000 * m_ramBankCount);
    }

    std::string title() const override
    {
        return getTitle(m_rom);
    }

    const ByteVector& ram() const override
    {
        return m_ram;
    }

    void setRam(const ByteVector& ram) override
    {
        if (ram.size() != m_ram.size())
        {
            throw IoException("saved cartridge ram size is not correct");
        }
        m_ram = ram;
    }

    void dump(Logger& log) const override
    {
        log() << "ROM:" << int_cast<int32>(getRomBank()) << "RAM:" << int_cast<int32>(getRamBank());
    }

    void write(const Address address, const Byte value) override
    {
        if (address < 0x2000)
        {
            const Byte lowBits = (value & 0x0F);
            m_allowRamAccess = (lowBits == 0x0A);
        }
        else if (address >= 0x2000 && address < 0x4000)
        {
            if (value != 0x00)
            {
                m_romBankLow = (value & 0x1F);
            }
            else
            {
                m_romBankLow = 0x01;
            }
        }
        else if (address >= 0x4000 && address < 0x6000)
        {
            m_modeBits = (value & 0b11);
        }
        else if (address >= 0x6000 && address < 0x8000)
        {
            setBankingMode(value);
        }
        else if (address >= 0xA000 && address < 0xC000)
        {
            if (!m_allowRamAccess)
            {
                return;
            }
            const uint16 bankAddress = (address - 0xA000);
            const uint8 ramBank = getRamBank();
            m_ram.at(ramBank * 0x2000 + bankAddress) = value;
        }
        else
        {
            throw InternalErrorException();
        }
    }

    Byte read(const Address address) const override
    {
        if (address < 0x4000)
        {
            return m_rom.at(address);
        }
        else if (address >= 0x4000 && address < 0x8000)
        {
            const uint16 bankAddress = address - 0x4000;
            const uint8 romBank = getRomBank();
            return m_rom.at(romBank * 0x4000 + bankAddress);
        }
        else if (address >= 0xA000 && address < 0xC000)
        {
            if (!m_allowRamAccess)
            {
                return 0xFF;
            }
            const uint16 ramAddress = (address - 0xA000);
            const uint8 ramBank = getRamBank();
            return m_ram.at(ramBank * 0x2000 + ramAddress);
        }
        throw InternalErrorException();
    }

private:
    void setBankingMode(const uint8 value)
    {
        if (value == 0x00)
        {
            m_mode = BankingMode::ROM;
        }
        else if (value == 0x01)
        {
            m_mode = BankingMode::RAM;
        }
        else
        {
            throw UnimplementedException("unknown MBC1 banking mode");
        }
    }

    uint8 getRomBank() const
    {
        if (m_mode == BankingMode::ROM)
        {
            const uint8 low = m_romBankLow;
            const uint8 high = int_cast<uint8>(m_modeBits << 5);
            return high | low;
        }
        if (m_mode == BankingMode::RAM)
        {
            const uint8 low = m_romBankLow;
            return low;
        }
        throw InternalErrorException();
    }

    uint8 getRamBank() const
    {
        if (m_mode == BankingMode::ROM)
        {
            return 0x00;
        }
        if (m_mode == BankingMode::RAM)
        {
            if (m_modeBits < m_ramBankCount)
            {
                return m_modeBits;
            }
            else
            {
                return 0x00;
            }
        }
        throw InternalErrorException();
    }

    enum class BankingMode : uint8
    {
        ROM,
        RAM,
    };
    BankingMode m_mode = BankingMode::ROM;
    uint8 m_modeBits = 0;

    ByteVector m_rom;
    uint8 m_romBankLow = 0x01;

    uint8 m_ramBankCount = 0;
    ByteVector m_ram;
    bool m_allowRamAccess = false;
};

class MemoryBankController2 final : public MemoryBankController
{
public:
    explicit MemoryBankController2(const ByteVector& rom)
        : m_rom(rom)
    {
        const uint8 romBankCount = getRomBankCount(rom);
        if (rom.size() != (0x4000 * romBankCount))
        {
            throw IoException("cartridge rom have incorrect size");
        }
        m_ram = createRandomBytes(0x200);
    }

    std::string title() const override
    {
        return getTitle(m_rom);
    }

    const ByteVector& ram() const override
    {
        return m_ram;
    }

    void setRam(const ByteVector& ram) override
    {
        if (ram.size() != m_ram.size())
        {
            throw IoException("saved cartridge ram size is not correct");
        }
        m_ram = ram;
    }

    void dump(Logger& log) const override
    {
        log() << "ROM:" << int_cast<int32>(m_romBank) << "RAM:" << 0;
    }

    void write(const Address address, const Byte value) override
    {
        if (address < 0x2000)
        {
            const uint8 highAddress = getHigh(address);
            if (isBitTrue(highAddress, 0))
            {
                const Byte lowBits = (value & 0x0F);
                m_allowRamAccess = (lowBits == 0x0A);
            }
        }
        else if (address >= 0x2000 && address < 0x4000)
        {
            const uint8 highAddress = getHigh(address);
            if (!isBitTrue(highAddress, 0))
            {
                return;
            }

            if (value != 0x00)
            {
                m_romBank = (value & 0x0F);
            }
            else
            {
                m_romBank = 0x01;
            }
        }
        else if (address >= 0xA000 && address < 0xA200)
        {
            if (!m_allowRamAccess)
            {
                return;
            }

            const uint16 ramAddress = (address - 0xA000);
            m_ram.at(ramAddress) = value & 0x0F;
        }
    }

    Byte read(const Address address) const override
    {
        if (address < 0x4000)
        {
            return m_rom.at(address);
        }
        else if (address >= 0x4000 && address < 0x8000)
        {
            const uint16 bankAddress = address - 0x4000;
            return m_rom.at(m_romBank * 0x4000 + bankAddress);
        }
        else if (address >= 0xA000 && address < 0xA200)
        {
            if (!m_allowRamAccess)
            {
                return 0xFF;
            }
            const uint16 ramAddress = (address - 0xA000);
            return m_ram.at(ramAddress) & 0x0F;
        }
        else
        {
            return 0xFF;
        }
    }

private:
    ByteVector m_rom;
    uint8 m_romBank = 0x01;

    ByteVector m_ram;
    bool m_allowRamAccess = false;
};

class MemoryBankController3 final : public MemoryBankController
{
public:
    explicit MemoryBankController3(const ByteVector& rom)
        : m_rom(rom)
    {
        const uint8 romBankCount = getRomBankCount(rom);
        if (rom.size() != (0x4000 * romBankCount))
        {
            throw IoException("cartridge rom have incorrect size");
        }
        const uint8 ramBankCount = getRamBankCount(rom);
        m_ram = createRandomBytes(0x2000 * ramBankCount);
    }

    std::string title() const override
    {
        return getTitle(m_rom);
    }

    const ByteVector& ram() const override
    {
        return m_ram;
    }

    void setRam(const ByteVector& ram) override
    {
        if (ram.size() != m_ram.size())
        {
            throw IoException("saved cartridge ram size is not correct");
        }
        m_ram = ram;
    }

    void dump(Logger& log) const override
    {
        log() << "ROM:" << int_cast<int32>(m_romBank) << "RAM:" << int_cast<int32>(m_ramBank);
    }

    void write(const Address address, const Byte value) override
    {
        if (address < 0x2000)
        {
            m_allowRamAccess = (value == 0x0A);
        }
        else if (address >= 0x2000 && address < 0x4000)
        {
            if (value == 0x00)
            {
                m_romBank = 0x01;
            }
            else
            {
                m_romBank = value;
            }
        }
        else if (address >= 0x4000 && address < 0x6000)
        {
            m_ramBank = value;
        }
        else if (address >= 0x6000 && address < 0x8000)
        {
            // Latch clock values, not needed ATM for this emulator.
        }
        else if (address >= 0xA000 && address < 0xC000)
        {
            if (!m_allowRamAccess)
            {
                return;
            }
            if (m_ramBank < 0x08)
            {
                const uint16 bankAddress = (address - 0xA000);
                m_ram.at(m_ramBank * 0x2000 + bankAddress) = value;
            }
            else if (m_ramBank == 0x08)
            {
                m_seconds = value;
            }
            else if (m_ramBank == 0x09)
            {
                m_minutes = value;
            }
            else if (m_ramBank == 0x0A)
            {
                m_hours = value;
            }
            else if (m_ramBank == 0xB0)
            {
                m_days = value;
            }
            else if (m_ramBank == 0x0C)
            {
                m_clockControl = value;
            }
            else
            {
                throw UnimplementedException("unknown MBC3 bank write");
            }
        }
        else
        {
            throw InternalErrorException();
        }
    }

    Byte read(const Address address) const override
    {
        if (address < 0x4000)
        {
            return m_rom.at(address);
        }
        else if (address >= 0x4000 && address < 0x8000)
        {
            const uint16 bankAddress = address - 0x4000;
            return m_rom.at(m_romBank * 0x4000 + bankAddress);
        }
        else if (address >= 0xA000 && address < 0xC000)
        {
            if (!m_allowRamAccess)
            {
                return 0xFF;
            }
            if (m_ramBank < 0x08)
            {
                const uint16 ramAddress = (address - 0xA000);
                return m_ram.at(m_ramBank * 0x2000 + ramAddress);
            }
            else if (m_ramBank == 0x08)
            {
                return int_cast<uint8>(m_seconds);
            }
            else if (m_ramBank == 0x09)
            {
                return int_cast<uint8>(m_minutes);
            }
            else if (m_ramBank == 0x0A)
            {
                return int_cast<uint8>(m_hours);
            }
            else if (m_ramBank == 0xB0)
            {
                return int_cast<uint8>(m_days & 0xFF);
            }
            else if (m_ramBank == 0x0C)
            {
                return m_clockControl;
            }
            else
            {
                throw UnimplementedException("unknown MBC3 bank read");
            }
        }
        throw InternalErrorException();
    }

private:
    ByteVector m_rom;
    uint8 m_romBank = 0x01;

    ByteVector m_ram;
    uint8 m_ramBank = 0;
    bool m_allowRamAccess = false;

    /**
     * The cartridge's real-time clock.
     */
    int32 m_seconds = 0;
    int32 m_minutes = 0;
    int32 m_hours = 0;
    int32 m_days = 0;
    uint8 m_clockControl = 0;
};

class MemoryBankController5 final : public MemoryBankController
{
public:
    explicit MemoryBankController5(const ByteVector& rom)
        : m_rom(rom)
    {
        const uint8 romBankCount = getRomBankCount(rom);
        if (rom.size() != (0x4000 * romBankCount))
        {
            throw IoException("cartridge rom have incorrect size");
        }
        const uint8 ramBankCount = getRamBankCount(rom);
        m_ram = createRandomBytes(0x2000 * ramBankCount);
    }

    std::string title() const override
    {
        return getTitle(m_rom);
    }

    const ByteVector& ram() const override
    {
        return m_ram;
    }

    void setRam(const ByteVector& ram) override
    {
        if (ram.size() != m_ram.size())
        {
            throw IoException("saved cartridge ram size is not correct");
        }
        m_ram = ram;
    }

    void dump(Logger& log) const override
    {
        log() << "ROM:" << int_cast<int32>(m_romBank) << "RAM:" << int_cast<int32>(m_ramBank);
    }

    void write(const Address address, const Byte value) override
    {
        if (address < 0x2000)
        {
            m_allowRamAccess = (value == 0x0A);
        }
        else if (address >= 0x2000 && address < 0x3000)
        {
            setLow(m_romBank, value);
        }
        else if (address >= 0x3000 && address < 0x4000)
        {
            setHigh(m_romBank, value & 0b01);
        }
        else if (address >= 0x4000 && address < 0x6000)
        {
            m_ramBank = value;
        }
        else if (address >= 0x6000 && address < 0x8000)
        {
            // Unused and empty space. Some games might write to this (Pokémon Yellow).
        }
        else if (address >= 0xA000 && address < 0xC000)
        {
            if (!m_allowRamAccess)
            {
                return;
            }
            const uint16 bankAddress = (address - 0xA000);
            m_ram.at(m_ramBank * 0x2000 + bankAddress) = value;
        }
        else
        {
            throw InternalErrorException();
        }
    }

    Byte read(const Address address) const override
    {
        if (address < 0x4000)
        {
            return m_rom.at(address);
        }
        else if (address >= 0x4000 && address < 0x8000)
        {
            const uint16 bankAddress = address - 0x4000;
            return m_rom.at(m_romBank * 0x4000 + bankAddress);
        }
        else if (address >= 0x6000 && address < 0x8000)
        {
            // Unused and empty space. Some games might read from this (Pokémon Yellow).
            return 0xFF;
        }
        else if (address >= 0xA000 && address < 0xC000)
        {
            if (!m_allowRamAccess)
            {
                return 0xFF;
            }
            const uint16 bankAddress = (address - 0xA000);
            return m_ram.at(m_ramBank * 0x2000 + bankAddress);
        }
        throw InternalErrorException();
    }

private:
    ByteVector m_rom;
    uint16 m_romBank = 0;

    ByteVector m_ram;
    uint8 m_ramBank = 0;
    bool m_allowRamAccess = false;
};

} // namespace

Cartridge::Cartridge(const ByteVector& rom)
{
    if (rom.size() < 0x8000)
    {
        throw IoException("cartridge rom is less than 32 KB");
    }

    switch (getCartridgeType(rom))
    {
    case CartridgeType::WithoutBank:
        m_controller = std::make_unique<MemoryBankControllerWithoutBank>(rom);
        break;
    case CartridgeType::MBC1:
        m_controller = std::make_unique<MemoryBankController1>(rom);
        break;
    case CartridgeType::MBC2:
        m_controller = std::make_unique<MemoryBankController2>(rom);
        break;
    case CartridgeType::MBC3:
        m_controller = std::make_unique<MemoryBankController3>(rom);
        break;
    case CartridgeType::MBC5:
        m_controller = std::make_unique<MemoryBankController5>(rom);
        break;
    default:
        throw UnimplementedException("unsupported cartridge type");
    }
}

void Cartridge::write(const Address address, const Byte value)
{
    m_controller->write(address, value);
}

Byte Cartridge::read(const Address address) const
{
    return m_controller->read(address);
}

std::string Cartridge::title() const
{
    return m_controller->title();
}

ByteVector Cartridge::ram() const
{
    return m_controller->ram();
}

void Cartridge::setRam(const ByteVector& ram)
{
    m_controller->setRam(ram);
}

void Cartridge::dump(Logger& log) const
{
    m_controller->dump(log);
}

} // namespace gb
