#pragma once

#include "gb_int.hpp"

#include <concepts>
#include <format>
#include <string>

namespace gb
{

/**
 * Convert an unsigned integer into a hex string (e.g. 0xFFA5).
 */
template<std::unsigned_integral Integer>
std::string toHex(const Integer value, const std::string_view prefix = "0x")
{
    const int64 hexCount = 2 * sizeof(Integer);
    return std::format("{}{:0{}X}", prefix, value, hexCount);
}

/**
 * Convert an unsigned integer into a binary string (e.g. 0b0011).
 */
template<std::unsigned_integral Integer>
std::string toBinary(const Integer value, const std::string_view prefix = "0b")
{
    const int64 bitCount = 8 * sizeof(Integer);
    return std::format("{}{:0{}b}", prefix, value, bitCount);
}

} // namespace gb
