#include "gb_lcd_system.hpp"
#include "gb_assert.hpp"
#include "gb_bit_utilities.hpp"
#include "gb_exception.hpp"
#include "gb_int_cast.hpp"

#include <algorithm>

namespace gb
{

LcdImage::LcdImage(const int32 width, const int32 height, Pixels pixels)
    : m_width(width), m_height(height), m_pixels(std::move(pixels))
{
    GB_ASSERT(m_width >= 0);
    GB_ASSERT(m_height >= 0);
    GB_ASSERT(m_pixels.size() == int_cast<uint64>(m_width * m_height));
}

int32 LcdImage::width() const
{
    return m_width;
}

int32 LcdImage::height() const
{
    return m_height;
}

const LcdImage::Pixels& LcdImage::pixels() const
{
    return m_pixels;
}

std::vector<uint8> LcdImage::toRgba() const
{
    std::vector<uint8> rgba;
    rgba.reserve(4 * m_pixels.size());
    for (const LcdColor& color : m_pixels)
    {
        rgba.push_back(color.red);
        rgba.push_back(color.green);
        rgba.push_back(color.blue);
        rgba.push_back(255);
    }
    return rgba;
}

LcdSystem::LcdSystem()
{
    /**
     * Game Boy programs expects the video RAM to be cleared on boot.
     */
    m_videoRam.resize(int_cast<uint64>(0xA000 - 0x8000), 0x00);
    m_oam.resize(int_cast<uint64>(0xFEA0 - 0xFE00), 0x00);
    m_pixels.resize(int_cast<uint64>(m_width * m_height));
}

LcdImage LcdSystem::getScreenImage() const
{
    return LcdImage(m_width, m_height, m_pixels);
}

LcdImage LcdSystem::getTileImage() const
{
    constexpr uint16 xDotCount = 32;
    constexpr uint16 yDotCount = 16;

    constexpr int32 width = 8 * xDotCount;
    constexpr int32 height = 8 * yDotCount;

    LcdImage::Pixels pixels(int_cast<uint64>(width * height));

    for (uint16 xDot = 0; xDot < xDotCount; xDot++)
    {
        for (uint16 yDot = 0; yDot < yDotCount; yDot++)
        {
            const uint16 dotIndex = xDot + xDotCount * yDot;
            const uint16 address = 0x8000 + 16 * dotIndex;
            for (uint8 x = 0; x < 8; x++)
            {
                for (uint8 y = 0; y < 8; y++)
                {
                    const int32 pixelX = 8 * xDot + x;
                    const int32 pixelY = 8 * yDot + y;
                    const int32 pixelIndex = pixelX + width * pixelY;
                    const LcdColor color = getBackgroundColorDot8x8(address, x, y);
                    pixels[int_cast<uint64>(pixelIndex)] = color;
                }
            }
        }
    }

    return LcdImage(width, height, pixels);
}

void LcdSystem::write(const Address address, const Byte value)
{
    if (address >= 0x8000 && address < 0xA000)
    {
        m_videoRam[address - 0x8000] = value;
    }
    else if (address >= 0xFE00 && address < 0xFEA0)
    {
        m_oam[address - 0xFE00] = value;
    }
    else if (address == 0xFF40)
    {
        m_drawControlRegister = value;
    }
    else if (address == 0xFF41)
    {
        m_interruptOnHBlank = isBitTrue(value, 3);
        m_interruptOnVBlank = isBitTrue(value, 4);
        m_interruptOnInOam = isBitTrue(value, 5);
        m_interruptOnLycMatch = isBitTrue(value, 6);
    }
    else if (address == 0xFF42)
    {
        m_backgroundScrollY = value;
    }
    else if (address == 0xFF43)
    {
        m_backgroundScrollX = value;
    }
    else if (address == 0xFF44)
    {
        m_ly = 0;
    }
    else if (address == 0xFF45)
    {
        m_lyc = value;
    }
    else if (address == 0xFF47)
    {
        m_backgroundPalette = value;
    }
    else if (address == 0xFF48)
    {
        m_spritePalette0 = value;
    }
    else if (address == 0xFF49)
    {
        m_spritePalette1 = value;
    }
    else if (address == 0xFF4A)
    {
        m_windowBackgroundY = value;
    }
    else if (address == 0xFF4B)
    {
        m_windowBackgroundX = int_cast<int32>(value) - 7;
    }
    else
    {
        throw UnimplementedException("unimplemented lcd write");
    }
}

Byte LcdSystem::read(const Address address) const
{
    if (address >= 0x8000 && address < 0xA000)
    {
        return m_videoRam.at(address - 0x8000);
    }
    else if (address >= 0xFE00 && address < 0xFEA0)
    {
        return m_oam.at(address - 0xFE00);
    }
    else if (address == 0xFF40)
    {
        return m_drawControlRegister;
    }
    else if (address == 0xFF41)
    {
        return getStatusRegister();
    }
    else if (address == 0xFF42)
    {
        return int_cast<Byte>(m_backgroundScrollY);
    }
    else if (address == 0xFF43)
    {
        return int_cast<Byte>(m_backgroundScrollX);
    }
    else if (address == 0xFF44)
    {
        return int_cast<Byte>(m_ly);
    }
    else if (address == 0xFF45)
    {
        return int_cast<Byte>(m_lyc);
    }
    else if (address == 0xFF47)
    {
        return m_backgroundPalette;
    }
    else if (address == 0xFF48)
    {
        return m_spritePalette0;
    }
    else if (address == 0xFF49)
    {
        return m_spritePalette1;
    }
    else if (address == 0xFF4A)
    {
        return int_cast<Byte>(m_windowBackgroundY);
    }
    else if (address == 0xFF4B)
    {
        return int_cast<Byte>(m_windowBackgroundX + 7);
    }
    else
    {
        throw UnimplementedException("unimplemented lcd read");
    }
}

bool LcdSystem::interrupt(const Cycles cycles)
{
    m_haveVBlankInterrupt = false;
    m_haveStatInterrupt = false;

    const bool lcdIsOn = isBitTrue(m_drawControlRegister, 7);
    if (!lcdIsOn)
    {
        return false;
    }

    m_modeClocksLeft -= cycles;

    if (m_modeClocksLeft <= 0)
    {
        // Mode transition.

        const Cycles leftOverClocks = -m_modeClocksLeft;

        if (m_mode == Mode::VBlank)
        {
            m_ly = 0;
            m_mode = Mode::InOAM;
            m_modeClocksLeft = 80 - leftOverClocks;
            m_haveStatInterrupt = (m_interruptOnInOam || haveLycInterrupt());
        }
        else if (m_mode == Mode::HBlank)
        {
            drawScanline(m_ly);
            m_ly += 1;

            if (m_ly < 144)
            {
                m_mode = Mode::InOAM;
                m_modeClocksLeft = 80 - leftOverClocks;
                m_haveStatInterrupt = m_interruptOnInOam;
            }
            else
            {
                m_mode = Mode::VBlank;
                m_modeClocksLeft = 4560 - leftOverClocks;
                m_modeVblanksClocksLeft = (456 - leftOverClocks);
                m_haveVBlankInterrupt = true;
                m_haveStatInterrupt = m_interruptOnVBlank;
            }

            m_haveStatInterrupt |= haveLycInterrupt();
        }
        else if (m_mode == Mode::InOAM)
        {
            m_mode = Mode::InOAMAndVideoRAM;
            m_modeClocksLeft = 172 - leftOverClocks;
        }
        else if (m_mode == Mode::InOAMAndVideoRAM)
        {
            m_mode = Mode::HBlank;
            m_modeClocksLeft = 204 - leftOverClocks;
            m_haveStatInterrupt = m_interruptOnHBlank;
        }
    }
    else if (m_mode == Mode::VBlank)
    {
        // Need to increment ly 10 times during VBlank.

        m_modeVblanksClocksLeft -= cycles;

        if (m_modeVblanksClocksLeft <= 0)
        {
            m_ly += 1;
            m_modeVblanksClocksLeft = (456 + m_modeVblanksClocksLeft);
            m_haveStatInterrupt = haveLycInterrupt();
        }
    }

    return m_haveVBlankInterrupt || m_haveStatInterrupt;
}

bool LcdSystem::haveVBlankInterrupt() const
{
    return m_haveVBlankInterrupt;
}

bool LcdSystem::haveStatInterrupt() const
{
    return m_haveStatInterrupt;
}

Byte LcdSystem::getStatusRegister() const
{
    uint8 status = 0;

    switch (m_mode)
    {
    case Mode::HBlank:
        status = 0b00;
        break;
    case Mode::VBlank:
        status = 0b01;
        break;
    case Mode::InOAM:
        status = 0b10;
        break;
    case Mode::InOAMAndVideoRAM:
        status = 0b11;
        break;
    }

    setBit(status, 2, m_ly == m_lyc);
    setBit(status, 3, m_interruptOnHBlank);
    setBit(status, 4, m_interruptOnVBlank);
    setBit(status, 5, m_interruptOnInOam);
    setBit(status, 6, m_interruptOnLycMatch);
    return status;
}

bool LcdSystem::haveLycInterrupt() const
{
    return m_interruptOnLycMatch && m_ly == m_lyc;
}

void LcdSystem::drawScanline(const int32 yPosition)
{
    const bool displayBackground = isBitTrue(m_drawControlRegister, 0);
    const bool displaySprites = isBitTrue(m_drawControlRegister, 1);
    const bool isDrawMode8x16 = isBitTrue(m_drawControlRegister, 2);
    const Address backgroundCodesArea = isBitTrue(m_drawControlRegister, 3) ? 0x9C00 : 0x9800;
    const Address backgroundCharacterArea = isBitTrue(m_drawControlRegister, 4) ? 0x8000 : 0x9000;
    const bool displayWindow = isBitTrue(m_drawControlRegister, 5);
    const Address windowCodesArea = isBitTrue(m_drawControlRegister, 6) ? 0x9C00 : 0x9800;

    if (displayBackground)
    {
        drawBackground(yPosition, backgroundCodesArea, backgroundCharacterArea);
    }
    if (displayWindow)
    {
        drawWindow(yPosition, windowCodesArea, backgroundCharacterArea);
    }
    if (displaySprites)
    {
        drawSprites(yPosition, isDrawMode8x16);
    }
}

void LcdSystem::drawBackground(const int32 yPosition, const Address codeArea, const Address characterArea)
{
    const int32 yLine = m_backgroundScrollY + yPosition;
    const int32 yDot = (yLine / 8) % 32;
    const uint8 yDotPixel = int_cast<uint8>(yLine % 8);

    for (int32 xPosition = 0; xPosition < 160; xPosition++)
    {
        const int32 xLine = m_backgroundScrollX + xPosition;
        const int32 xDot = (xLine / 8) % 32;
        const uint8 xDotPixel = int_cast<uint8>(xLine % 8);

        const Address tileAddress = getBackgroundTileAddress(codeArea, characterArea, xDot, yDot);
        const LcdColor rgb = getBackgroundColorDot8x8(tileAddress, xDotPixel, yDotPixel);

        const int32 pixelIndex = xPosition + yPosition * 160;
        m_pixels[int_cast<uint64>(pixelIndex)] = rgb;
    }
}

void LcdSystem::drawWindow(const int32 yPosition, const Address codeArea, const Address characterArea)
{
    const int32 yLine = yPosition - m_windowBackgroundY;
    if (yLine < 0 || yLine > 143)
    {
        return;
    }
    const int32 yDot = (yLine / 8);
    const uint8 yDotPixel = int_cast<uint8>(yLine % 8);

    for (int32 xPosition = 0; xPosition < 160; xPosition++)
    {
        const int32 xLine = xPosition - m_windowBackgroundX;
        if (xLine < 0 || xLine > 159)
        {
            continue;
        }
        const int32 xDot = (xLine / 8);
        const uint8 xDotPixel = int_cast<uint8>(xLine % 8);

        const Address tileAddress = getBackgroundTileAddress(codeArea, characterArea, xDot, yDot);
        const LcdColor rgb = getBackgroundColorDot8x8(tileAddress, xDotPixel, yDotPixel);

        const int32 pixelIndex = xPosition + yPosition * 160;
        m_pixels[int_cast<uint64>(pixelIndex)] = rgb;
    }
}

static LcdColor colorBitsToRgb(const uint8 palette, const uint8 colorBits)
{
    /**
     * The color bits is the 2-bit index into the palette.
     */
    const int32 paletteIndex = (colorBits & 0b11);

    /**
     * The palette maps 4 indices to 4 greyscale colors.
     */
    const int32 colorIndex = ((palette >> (2 * paletteIndex)) & 0b11);

    if (colorIndex == 0b00)
    {
        return LcdColor{.red = 255, .green = 255, .blue = 255};
    }
    if (colorIndex == 0b01)
    {
        return LcdColor{.red = 192, .green = 192, .blue = 192};
    }
    if (colorIndex == 0b10)
    {
        return LcdColor{.red = 96, .green = 96, .blue = 96};
    }
    if (colorIndex == 0b11)
    {
        return LcdColor{.red = 0, .green = 0, .blue = 0};
    }
    throw InternalErrorException();
}

static bool operator==(const LcdColor& a, const LcdColor& b)
{
    return a.red == b.red && a.blue == b.blue && a.green == b.green;
}

void LcdSystem::drawSprites(const int32 yPosition, const bool isDrawMode8x16)
{
    for (const Sprite& sprite : getSpritesInScanline(yPosition, isDrawMode8x16))
    {
        const int32 y = yPosition - sprite.yPosition;
        constexpr int32 spriteByteSize = 16;
        const Address spriteAddress = int_cast<Address>(0x8000 + spriteByteSize * sprite.code);

        const bool spriteIsInFrontOfBackground = !isBitTrue(sprite.attribute, 7);
        const bool flipY = isBitTrue(sprite.attribute, 6);
        const bool flipX = isBitTrue(sprite.attribute, 5);
        const bool usePalette1 = isBitTrue(sprite.attribute, 4);

        for (int32 x = 0; x < 8; x++)
        {
            const int32 xPosition = sprite.xPosition + x;
            if (xPosition < 0 || xPosition > 159)
            {
                continue;
            }

            const uint8 flippedX = int_cast<uint8>(flipX ? (7 - x) : x);
            const uint8 flippedY = int_cast<uint8>(flipY ? (7 - y) : y);
            const uint8 colorBits = getColorBitsInDot8x8(spriteAddress, flippedX, flippedY);

            /**
             * 0b00 is transparent for sprites.
             */
            if (colorBits == 0b00)
            {
                continue;
            }

            /**
             * Sprites are always in front of background color 0.
             */
            const uint64 pixelIndex = int_cast<uint64>(xPosition + yPosition * 160);
            const LcdColor bgColor0 = colorBitsToRgb(m_backgroundPalette, 0b00);
            const bool haveBgColor0 = (bgColor0 == m_pixels[pixelIndex]);

            if (spriteIsInFrontOfBackground || haveBgColor0)
            {
                const uint8 palette = usePalette1 ? m_spritePalette1 : m_spritePalette0;
                m_pixels[int_cast<uint64>(pixelIndex)] = colorBitsToRgb(palette, colorBits);
            }
        }
    }
}

uint8 LcdSystem::getColorBitsInDot8x8(const Address address, const uint8 x, const uint8 y) const
{
    GB_ASSERT(x <= 7);
    GB_ASSERT(y <= 7);

    /**
     * One 8x8 tile uses 16 bytes for the 64 pixels. Each pixel gets 2 bits of color information.
     */
    const Address start = address + 2 * y;
    const uint8 lowBits = m_videoRam[start - 0x8000];
    const uint8 highBits = m_videoRam[start + 1 - 0x8000];

    /**
     * The 2 color bits for e.g. x=0 is the concatenation of the 7th bit in the lowest byte
     * with the 7th bit in the highest byte. And similar for the other x values.
     */
    const uint8 bitIndex = 7 - x;
    uint8 colorBits = 0;
    setBit(colorBits, 0, isBitTrue(lowBits, bitIndex));
    setBit(colorBits, 1, isBitTrue(highBits, bitIndex));

    return colorBits;
}

LcdColor LcdSystem::getBackgroundColorDot8x8(const Address address, const uint8 x, const uint8 y) const
{
    const uint8 colorBits = getColorBitsInDot8x8(address, x, y);
    return colorBitsToRgb(m_backgroundPalette, colorBits);
}

Address LcdSystem::getBackgroundTileAddress(const Address codeArea,
                                            const Address characterArea,
                                            const int32 xDot,
                                            const int32 yDot) const
{
    /**
     * Lookup the code of the tile in the code area.
     */
    const int32 codeIndex = xDot + yDot * 32;
    const int32 codeAddress = codeArea + codeIndex;
    const uint8 code = m_videoRam[int_cast<uint64>(codeAddress - 0x8000)];

    /**
     * Map the code to the tile address. There are 3 blocks of tiles:
     *    Block 0: 0x8000-0x87FF.
     *    Block 1: 0x8800-0x8FFF.
     *    Block 2: 0x9000-0x97FF.
     * When tile code is 0-0x7F it is mapped to either block 0 or 2.
     * When tile code is 0x80-0xFF it is mapped to block 1.
     */
    constexpr int32 tileByteSize = 16;
    if (code >= 0x80)
    {
        const Address characterOffset = tileByteSize * (code - 0x80);
        return 0x8800 + characterOffset;
    }
    else
    {
        const Address characterOffset = tileByteSize * code;
        return characterArea + characterOffset;
    }
}

std::vector<LcdSystem::Sprite> LcdSystem::getSpritesInScanline(const int32 yPosition, const bool isDrawMode8x16) const
{
    std::vector<Sprite> result;

    /**
     * Find all sprites that overlap the scanline.
     */
    for (int32 objIndex = 0; objIndex < 40; objIndex++)
    {
        const uint64 offset = int_cast<uint64>(4 * objIndex);

        const int32 spriteY = m_oam[offset + 0] - 16;
        const int32 spriteX = m_oam[offset + 1] - 8;
        const uint8 spriteCode = m_oam[offset + 2];
        const uint8 spriteAttribute = m_oam[offset + 3];

        const int32 spriteHeight = isDrawMode8x16 ? 16 : 8;
        const int32 spriteYMin = spriteY;
        const int32 spriteYMax = spriteY + spriteHeight - 1;

        if (yPosition >= spriteYMin && yPosition <= spriteYMax)
        {
            const Sprite sprite{.xPosition = spriteX,
                                .yPosition = spriteY,
                                .code = spriteCode,
                                .attribute = spriteAttribute};
            const Sprite adjustedSprite = isDrawMode8x16 ? adjustSpriteFor8x16Mode(sprite, yPosition) : sprite;
            result.push_back(adjustedSprite);
        }
    }

    /**
     * Game Boy draws sprites with lowest x first and prioritizes those.
     */
    const auto xIsLessThan = [](const Sprite& a, const Sprite& b)
    {
        return a.xPosition < b.xPosition;
    };
    std::ranges::stable_sort(result, xIsLessThan);

    /**
     * Game Boy cannot draw more than 10 sprites.
     */
    if (result.size() > 10)
    {
        result.erase(result.begin() + 10, result.end());
    }

    return result;
}

LcdSystem::Sprite LcdSystem::adjustSpriteFor8x16Mode(const Sprite sprite, const int32 yPosition)
{
    /**
     * In 8x16 sprite draw mode a sprite represents two 8x8 sprites on top of each other.
     * When we draw a 8x16 sprite we determine which of the two 8x8 sprites to draw.
     */
    const int32 yIndex = yPosition - sprite.yPosition;
    const bool flipY = isBitTrue(sprite.attribute, 6);
    const uint8 spriteCodeLow = sprite.code & 0b11111110;
    const uint8 spriteCodeHigh = sprite.code | 0b00000001;

    Sprite adjustedSprite = sprite;
    if (yIndex < 8)
    {
        adjustedSprite.yPosition = sprite.yPosition;
        adjustedSprite.code = flipY ? spriteCodeHigh : spriteCodeLow;
    }
    else
    {
        adjustedSprite.yPosition = sprite.yPosition + 8;
        adjustedSprite.code = flipY ? spriteCodeLow : spriteCodeHigh;
    }
    return adjustedSprite;
}

} // namespace gb
