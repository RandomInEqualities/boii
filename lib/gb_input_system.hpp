#pragma once

#include "gb_int.hpp"

#include <map>

namespace gb
{

/**
 * Input keys that the Game Boy has.
 */
enum class Key : uint8
{
    Right,
    Left,
    Up,
    Down,
    A,
    B,
    Select,
    Start
};

/**
 * System for button press input to the Game Boy.
 */
class InputSystem
{
public:
    explicit InputSystem();

    /**
     * Set if the given key is pressed down or not.
     */
    void setPressed(Key key, bool isPressed);

    /**
     * Write the specified byte to the specified address.
     */
    void write(Address, Byte);

    /**
     * Read the byte at the specified address.
     */
    Byte read(Address) const;

    /**
     * Check if the input system has an interrupt.
     */
    bool interrupt(Cycles);

private:
    std::map<Key, bool> m_isPressed;
    bool m_useDirectionKeys = false;
    bool m_useSelectionKeys = false;
    bool m_haveInterrupt = false;
};

} // namespace gb
