#pragma once

#include "gb_exception.hpp"

/**
 * Assert macro that throws an AssertException if it fails.
 *
 * By throwing an exception the assertions can be tested.
 */
#define GB_ASSERT(condition)                       \
    do                                             \
    {                                              \
        if (!(condition))                          \
        {                                          \
            throw gb::AssertException(#condition); \
        }                                          \
    } while (false)
