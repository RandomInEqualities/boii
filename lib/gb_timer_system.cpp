#include "gb_timer_system.hpp"
#include "gb_bit_utilities.hpp"
#include "gb_constants.hpp"
#include "gb_exception.hpp"

namespace gb
{

constexpr Cycles getResetCycle(const int32 frequency)
{
    const Cycles cyclesPerIncrement = constant::cpuCyclesPerSecond / frequency;
    return cyclesPerIncrement;
}

TimerSystem::TimerSystem()
    : m_divCycleCounter(getResetCycle(16384)), m_timaCycleCounter(getResetCycle(4096))
{
}

void TimerSystem::write(const Address address, const Byte value)
{
    if (address == 0xFF04)
    {
        m_divCounter = 0;
    }
    else if (address == 0xFF05)
    {
        m_timaCounter = value;
    }
    else if (address == 0xFF06)
    {
        m_timaReset = value;
    }
    else if (address == 0xFF07)
    {
        // Only the lower 3 bits are used, the other bits are set to 1.
        m_tac = (value | 0b11111000);

        const Byte lowBits = (value & 0b11);
        if (lowBits == 0b00)
        {
            m_timaCycleCounter.setResetCycle(getResetCycle(4096));
        }
        else if (lowBits == 0b01)
        {
            m_timaCycleCounter.setResetCycle(getResetCycle(262144));
        }
        else if (lowBits == 0b10)
        {
            m_timaCycleCounter.setResetCycle(getResetCycle(65536));
        }
        else if (lowBits == 0b11)
        {
            m_timaCycleCounter.setResetCycle(getResetCycle(16385));
        }

        m_timaCycleCounter.setEnabled(isBitTrue(value, 2));
    }
    else
    {
        throw InternalErrorException();
    }
}

Byte TimerSystem::read(const Address address) const
{
    if (address == 0xFF04)
    {
        return int_cast<Byte>(m_divCounter);
    }
    else if (address == 0xFF05)
    {
        return int_cast<Byte>(m_timaCounter);
    }
    else if (address == 0xFF06)
    {
        return int_cast<Byte>(m_timaReset);
    }
    else if (address == 0xFF07)
    {
        return m_tac;
    }
    else
    {
        throw InternalErrorException();
    }
}

bool TimerSystem::interrupt(const Cycles cycles)
{
    stepDiv(cycles);
    return stepTima(cycles);
}

bool TimerSystem::stepDiv(const Cycles cycles)
{
    if (m_divCycleCounter.step(cycles))
    {
        m_divCounter += 1;
        if (m_divCounter > 255)
        {
            m_divCounter = 0;
            return true;
        }
    }
    return false;
}

bool TimerSystem::stepTima(const Cycles cycles)
{
    if (m_timaCycleCounter.step(cycles))
    {
        m_timaCounter += 1;
        if (m_timaCounter > 255)
        {
            m_timaCounter = m_timaReset;
            return true;
        }
    }
    return false;
}

} // namespace gb
