cmake_minimum_required(VERSION 3.28)
project(gameboi CXX)
enable_testing()

#
# Game Boy build helpers.
#
add_subdirectory(build)

#
# Game Boy executable.
#
add_subdirectory(app)

#
# Game Boy dependencies.
#
add_subdirectory(dependencies)

#
# Game Boy library.
#
add_subdirectory(lib)

#
# Game Boy tests.
#
add_subdirectory(tests)
