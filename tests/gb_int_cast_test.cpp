#include "gb_exception.hpp"
#include "gb_int_cast.hpp"

#include <catch2/catch_test_macros.hpp>

#include <cstdint>
#include <limits>

/**
 * Tests for the int_cast conversion function.
 */

TEST_CASE("int_cast")
{
    using i8 = std::int8_t;
    using i32 = std::int32_t;
    using u8 = std::uint8_t;
    using u32 = std::uint32_t;
    using gb::AssertException;
    using gb::int_cast;

    SECTION("convert int32")
    {
        // int32 to int8
        CHECK_THROWS_AS(int_cast<i8>(i32{-129}), AssertException);
        CHECK(int_cast<i8>(i32{-128}) == i8{-128});
        CHECK(int_cast<i8>(i32{0}) == i8{0});
        CHECK(int_cast<i8>(i32{127}) == i8{127});
        CHECK_THROWS_AS(int_cast<i8>(i32{128}), AssertException);

        // int32 to uint8
        CHECK_THROWS_AS(int_cast<u8>(i32{-1}), AssertException);
        CHECK(int_cast<u8>(i32{0}) == u8{0});
        CHECK(int_cast<u8>(i32{255}) == u8{255});
        CHECK_THROWS_AS(int_cast<u8>(i32{256}), AssertException);

        // int32 to uint32
        constexpr i32 i32Max = std::numeric_limits<i32>::max();
        CHECK_THROWS_AS(int_cast<u32>(i32{-1}), AssertException);
        CHECK(int_cast<u32>(i32{0}) == u32{0});
        CHECK(int_cast<u32>(i32{i32Max}) == u32{i32Max});

        // int32 to int32
        constexpr i32 i32Min = std::numeric_limits<i32>::lowest();
        CHECK(int_cast<i32>(i32{i32Min}) == i32{i32Min});
        CHECK(int_cast<i32>(i32{0}) == i32{0});
        CHECK(int_cast<i32>(i32{i32Max}) == i32{i32Max});
    }

    SECTION("convert uint32")
    {
        // uint32 to int8
        CHECK(int_cast<i8>(u32{0}) == i8{0});
        CHECK(int_cast<i8>(u32{127}) == i8{127});
        CHECK_THROWS_AS(int_cast<i8>(u32{128}), AssertException);

        // uint32 to uint8
        CHECK(int_cast<u8>(u32{0}) == u8{0});
        CHECK(int_cast<u8>(u32{255}) == u8{255});
        CHECK_THROWS_AS(int_cast<u8>(u32{256}), AssertException);

        // uint32 to int32
        constexpr i32 i32Max = std::numeric_limits<i32>::max();
        CHECK(int_cast<i32>(u32{0}) == i32{0});
        CHECK(int_cast<i32>(u32{i32Max}) == i32{i32Max});
        CHECK_THROWS_AS(int_cast<i32>(u32{i32Max} + 1), AssertException);

        // uint32 to uint32
        constexpr u32 u32Max = std::numeric_limits<u32>::max();
        CHECK(int_cast<u32>(u32{0}) == u32{0});
        CHECK(int_cast<u32>(u32{u32Max}) == u32{u32Max});
    }

    SECTION("convert int8")
    {
        // int8 to int32
        CHECK(int_cast<i32>(i8{-128}) == i32{-128});
        CHECK(int_cast<i32>(i8{127}) == i32{127});

        // int8 to uint32
        CHECK_THROWS_AS(int_cast<u32>(i8{-128}), AssertException);
        CHECK_THROWS_AS(int_cast<u32>(i8{-1}), AssertException);
        CHECK(int_cast<u32>(i8{0}) == u32{0});
        CHECK(int_cast<u32>(i8{127}) == u32{127});

        // int8 to int8
        CHECK(int_cast<i8>(i8{-128}) == i8{-128});
        CHECK(int_cast<i8>(i8{127}) == i8{127});
    }

    SECTION("convert uint8")
    {
        // uint8 to int32
        CHECK(int_cast<i32>(u8{0}) == i32{0});
        CHECK(int_cast<i32>(u8{255}) == i32{255});

        // uint8 to uint32
        CHECK(int_cast<u32>(u8{0}) == u32{0});
        CHECK(int_cast<u32>(u8{255}) == u32{255});

        // uint8 to uint8
        CHECK(int_cast<u8>(u8{0}) == u8{0});
        CHECK(int_cast<u8>(u8{255}) == u8{255});
    }
}
