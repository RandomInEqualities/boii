#include "gb_int.hpp"
#include "gb_string_utilities.hpp"

#include <catch2/catch_test_macros.hpp>

/**
 * Tests for the string utility functions.
 */

TEST_CASE("toHex")
{
    SECTION("convert uint8")
    {
        CHECK(gb::toHex(gb::uint8{0u}) == "0x00");
        CHECK(gb::toHex(gb::uint8{134u}) == "0x86");
        CHECK(gb::toHex(gb::uint8{255u}) == "0xFF");
    }

    SECTION("convert uint16")
    {
        CHECK(gb::toHex(gb::uint16{0u}) == "0x0000");
        CHECK(gb::toHex(gb::uint16{8345u}) == "0x2099");
        CHECK(gb::toHex(gb::uint16{65535u}) == "0xFFFF");
    }

    SECTION("convert uint32")
    {
        CHECK(gb::toHex(gb::uint32{0u}) == "0x00000000");
        CHECK(gb::toHex(gb::uint32{911762763u}) == "0x3658654B");
        CHECK(gb::toHex(gb::uint32{4294967295u}) == "0xFFFFFFFF");
    }

    SECTION("convert uint64")
    {
        CHECK(gb::toHex(gb::uint64{0u}) == "0x0000000000000000");
        CHECK(gb::toHex(gb::uint64{9117627639117627631u}) == "0x7E885207CFE3C0EF");
        CHECK(gb::toHex(gb::uint64{18446744073709551615u}) == "0xFFFFFFFFFFFFFFFF");
    }

    SECTION("prefix")
    {
        CHECK(gb::toHex(gb::uint16{0u}, "test") == "test0000");
        CHECK(gb::toHex(gb::uint16{8345u}, "test") == "test2099");
        CHECK(gb::toHex(gb::uint16{65535u}, "test") == "testFFFF");
    }
}

TEST_CASE("toBinary")
{
    SECTION("convert uint8")
    {
        CHECK(gb::toBinary(gb::uint8{0u}) == "0b00000000");
        CHECK(gb::toBinary(gb::uint8{134u}) == "0b10000110");
        CHECK(gb::toBinary(gb::uint8{255u}) == "0b11111111");
    }

    SECTION("convert uint16")
    {
        CHECK(gb::toBinary(gb::uint16{0u}) == "0b0000000000000000");
        CHECK(gb::toBinary(gb::uint16{8345u}) == "0b0010000010011001");
        CHECK(gb::toBinary(gb::uint16{65535u}) == "0b1111111111111111");
    }

    SECTION("convert uint32")
    {
        CHECK(gb::toBinary(gb::uint32{0u}) == "0b00000000000000000000000000000000");
        CHECK(gb::toBinary(gb::uint32{911762763u}) == "0b00110110010110000110010101001011");
        CHECK(gb::toBinary(gb::uint32{4294967295u}) == "0b11111111111111111111111111111111");
    }

    SECTION("convert uint64")
    {
        CHECK(gb::toBinary(gb::uint64{0000000000000000000u})
              == "0b0000000000000000000000000000000000000000000000000000000000000000");
        CHECK(gb::toBinary(gb::uint64{9117627639117627631u})
              == "0b0111111010001000010100100000011111001111111000111100000011101111");
        CHECK(gb::toBinary(gb::uint64{18446744073709551615u})
              == "0b1111111111111111111111111111111111111111111111111111111111111111");
    }

    SECTION("prefix")
    {
        CHECK(gb::toBinary(gb::uint8{0u}, "test") == "test00000000");
        CHECK(gb::toBinary(gb::uint8{134u}, "test") == "test10000110");
        CHECK(gb::toBinary(gb::uint8{255u}, "test") == "test11111111");
    }
}
