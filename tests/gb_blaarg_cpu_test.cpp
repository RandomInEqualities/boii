#include "gb_byte.hpp"
#include "gb_cartridge.hpp"
#include "gb_constants.hpp"
#include "gb_processor.hpp"

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include <filesystem>
#include <string>

/**
 * This tests runs Blaarg's Game Boy CPU instruction test cartridges. These cartridges run through the entire Game
 * Boy instruction set and outputs "passed" to the serial port at the end if everything executed as expected.
 *
 * The cartridges are from here under the cpu_instrs folder: https://github.com/L-P/blargg-test-roms
 */

TEST_CASE("Blaarg's CPU instruction cartridges")
{
    struct TestInput
    {
        std::string_view cartridgeFilename;
        std::string_view expectedSerialOutput;
    };
    const TestInput input = GENERATE(TestInput("01-special.gb", "01-special\n\n\nPassed\n"),
                                     TestInput("02-interrupts.gb", "02-interrupts\n\n\nPassed\n"),
                                     TestInput("03-op sp,hl.gb", "03-op sp,hl\n\n\nPassed\n"),
                                     TestInput("04-op r,imm.gb", "04-op r,imm\n\n\nPassed\n"),
                                     TestInput("05-op rp.gb", "05-op rp\n\n\nPassed\n"),
                                     TestInput("06-ld r,r.gb", "06-ld r,r\n\n\nPassed\n"),
                                     TestInput("07-jr,jp,call,ret,rst.gb", "07-jr,jp,call,ret,rst\n\n\nPassed\n"),
                                     TestInput("08-misc instrs.gb", "08-misc instrs\n\n\nPassed\n"),
                                     TestInput("09-op r,r.gb", "09-op r,r\n\n\nPassed\n"),
                                     TestInput("10-bit ops.gb", "10-bit ops\n\n\nPassed\n"),
                                     TestInput("11-op a,(hl).gb", "11-op a,(hl)\n\n\nPassed\n"));

    DYNAMIC_SECTION("cartridge: " << input.cartridgeFilename)
    {
        const std::filesystem::path thisDirectory = std::filesystem::path(__FILE__).parent_path();
        const std::filesystem::path cartridgePath = thisDirectory / "blaarg_cpu_cartridges" / input.cartridgeFilename;
        REQUIRE(std::filesystem::is_regular_file(cartridgePath));

        /**
         * Run the Game Boy processor with the cartridge.
         */
        gb::Cartridge cartridge(gb::readBytesFromFile(cartridgePath));
        gb::Processor processor(cartridge);

        /**
         * The cartridge writes to the serial port, emulating a console.
         */
        std::string serialPortOutput;
        processor.serial().setTransferNotifier([&](const gb::Byte byte)
                                               { serialPortOutput.push_back(gb::int_cast<char>(byte)); });

        /**
         * Run the processor until the expected output occur or until timeout.
         */
        constexpr gb::Cycles cyclesForTimeout = 25 * gb::constant::cpuCyclesPerSecond;
        gb::Cycles cycleCount = 0;
        while (true)
        {
            const gb::Cycles newCycles = processor.step(gb::nullLog);
            cycleCount += newCycles;

            if (input.expectedSerialOutput == serialPortOutput)
            {
                SUCCEED("Test has received expected output: " << serialPortOutput);
                break;
            }

            if (cycleCount > cyclesForTimeout)
            {
                FAIL("Test has timed out, output received: " << serialPortOutput);
                break;
            }
        }
    }
}
