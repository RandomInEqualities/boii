#include "gb_bit_utilities.hpp"

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

/**
 * Tests for the low level bit utilities.
 */

TEST_CASE("getLow")
{
    CHECK(gb::getLow(0x0000) == 0x00);
    CHECK(gb::getLow(0xFFFF) == 0xFF);
    CHECK(gb::getLow(0x00FF) == 0xFF);
    CHECK(gb::getLow(0xFF00) == 0x00);
    CHECK(gb::getLow(0x1234) == 0x34);
}

TEST_CASE("getHigh")
{
    CHECK(gb::getHigh(0x0000) == 0x00);
    CHECK(gb::getHigh(0xFFFF) == 0xFF);
    CHECK(gb::getHigh(0x00FF) == 0x00);
    CHECK(gb::getHigh(0xFF00) == 0xFF);
    CHECK(gb::getHigh(0x1234) == 0x12);
}

TEST_CASE("setLow")
{
    gb::uint16 value = GENERATE(gb::uint16{0x0000},
                                gb::uint16{0xFFFF},
                                gb::uint16{0xFF00},
                                gb::uint16{0x00FF},
                                gb::uint16{0xEE00},
                                gb::uint16{0x00EE},
                                gb::uint16{0x5555},
                                gb::uint16{0x1234},
                                gb::uint16{0x3412});

    const gb::uint8 highBits = gb::getHigh(value);

    SECTION("set low to 0")
    {
        gb::setLow(value, 0);
        REQUIRE(gb::getLow(value) == 0);
        REQUIRE(gb::getHigh(value) == highBits);
    }

    SECTION("set low to 255")
    {
        gb::setLow(value, 255);
        REQUIRE(gb::getLow(value) == 255);
        REQUIRE(gb::getHigh(value) == highBits);
    }

    SECTION("set low to 128")
    {
        gb::setLow(value, 128);
        REQUIRE(gb::getLow(value) == 128);
        REQUIRE(gb::getHigh(value) == highBits);
    }

    SECTION("set low to 55")
    {
        gb::setLow(value, 55);
        REQUIRE(gb::getLow(value) == 55);
        REQUIRE(gb::getHigh(value) == highBits);
    }
}

TEST_CASE("setHigh")
{
    gb::uint16 value = GENERATE(gb::uint16{0x0000},
                                gb::uint16{0xFFFF},
                                gb::uint16{0xFF00},
                                gb::uint16{0x00FF},
                                gb::uint16{0xEE00},
                                gb::uint16{0x00EE},
                                gb::uint16{0x5555},
                                gb::uint16{0x1234},
                                gb::uint16{0x3412});

    const gb::uint8 lowBits = gb::getLow(value);

    SECTION("set high to 0")
    {
        gb::setHigh(value, 0);
        REQUIRE(gb::getLow(value) == lowBits);
        REQUIRE(gb::getHigh(value) == 0);
    }

    SECTION("set high to 255")
    {
        gb::setHigh(value, 255);
        REQUIRE(gb::getLow(value) == lowBits);
        REQUIRE(gb::getHigh(value) == 255);
    }

    SECTION("set high to 128")
    {
        gb::setHigh(value, 128);
        REQUIRE(gb::getLow(value) == lowBits);
        REQUIRE(gb::getHigh(value) == 128);
    }

    SECTION("set high to 30")
    {
        gb::setHigh(value, 30);
        REQUIRE(gb::getLow(value) == lowBits);
        REQUIRE(gb::getHigh(value) == 30);
    }
}

TEST_CASE("isBitTrue")
{
    using gb::isBitTrue;

    CHECK(!isBitTrue(0x0000, 0));
    CHECK(!isBitTrue(0x0000, 1));
    CHECK(!isBitTrue(0x0000, 7));
    CHECK(!isBitTrue(0x0000, 8));
    CHECK(!isBitTrue(0x0000, 15));

    CHECK(isBitTrue(0xFFFF, 0));
    CHECK(isBitTrue(0xFFFF, 1));
    CHECK(isBitTrue(0xFFFF, 7));
    CHECK(isBitTrue(0xFFFF, 8));
    CHECK(isBitTrue(0xFFFF, 15));

    CHECK(isBitTrue(0b1110010100010111, 0));
    CHECK(isBitTrue(0b1110010100010111, 1));
    CHECK(isBitTrue(0b1110010100010111, 2));
    CHECK(!isBitTrue(0b1110010100010111, 3));
    CHECK(isBitTrue(0b1110010100010111, 4));
    CHECK(!isBitTrue(0b1110010100010111, 5));
    CHECK(!isBitTrue(0b1110010100010111, 6));
    CHECK(!isBitTrue(0b1110010100010111, 7));
    CHECK(isBitTrue(0b1110010100010111, 8));
    CHECK(!isBitTrue(0b1110010100010111, 9));
    CHECK(isBitTrue(0b1110010100010111, 10));
    CHECK(!isBitTrue(0b1110010100010111, 11));
    CHECK(!isBitTrue(0b1110010100010111, 12));
    CHECK(isBitTrue(0b1110010100010111, 13));
    CHECK(isBitTrue(0b1110010100010111, 14));
    CHECK(isBitTrue(0b1110010100010111, 15));
}

TEST_CASE("setBit")
{
    using gb::setBit;

    gb::uint16 value = 0xFFFF;
    REQUIRE(value == 0xFFFF);

    // set low bits to true
    setBit(value, 0, false);
    REQUIRE(value == 0xFFFE);
    setBit(value, 1, false);
    REQUIRE(value == 0xFFFC);
    setBit(value, 2, false);
    REQUIRE(value == 0xFFF8);

    // set low bits to false
    setBit(value, 2, true);
    REQUIRE(value == 0xFFFC);
    setBit(value, 1, true);
    REQUIRE(value == 0xFFFE);
    setBit(value, 0, true);
    REQUIRE(value == 0xFFFF);

    // set high bits to true
    setBit(value, 15, false);
    REQUIRE(value == 0x7FFF);
    setBit(value, 14, false);
    REQUIRE(value == 0x3FFF);
    setBit(value, 13, false);
    REQUIRE(value == 0x1FFF);

    // set high bits to false
    setBit(value, 13, true);
    REQUIRE(value == 0x3FFF);
    setBit(value, 14, true);
    REQUIRE(value == 0x7FFF);
    setBit(value, 15, true);
    REQUIRE(value == 0xFFFF);

    // set middle bits to true / false
    setBit(value, 7, false);
    REQUIRE(value == 0xFF7F);
    setBit(value, 7, true);
    REQUIRE(value == 0xFFFF);
    setBit(value, 8, false);
    REQUIRE(value == 0xFEFF);
    setBit(value, 8, true);
    REQUIRE(value == 0xFFFF);
}

TEST_CASE("getOneBits")
{
    using gb::getOneBits;

    CHECK(getOneBits<1>() == 0b00000001);
    CHECK(getOneBits<2>() == 0b00000011);
    CHECK(getOneBits<3>() == 0b00000111);
    CHECK(getOneBits<4>() == 0b00001111);
    CHECK(getOneBits<5>() == 0b00011111);
    CHECK(getOneBits<6>() == 0b00111111);
    CHECK(getOneBits<7>() == 0b01111111);
    CHECK(getOneBits<8>() == 0b11111111);
}

TEST_CASE("getOffsetBits")
{
    using gb::getOffsetBits;

    CHECK(getOffsetBits<1, 0>(0b11111111) == 0b00000001);
    CHECK(getOffsetBits<2, 0>(0b11111111) == 0b00000011);
    CHECK(getOffsetBits<3, 0>(0b11111111) == 0b00000111);
    CHECK(getOffsetBits<4, 0>(0b11111111) == 0b00001111);
    CHECK(getOffsetBits<5, 0>(0b11111111) == 0b00011111);
    CHECK(getOffsetBits<6, 0>(0b11111111) == 0b00111111);
    CHECK(getOffsetBits<7, 0>(0b11111111) == 0b01111111);
    CHECK(getOffsetBits<8, 0>(0b11111111) == 0b11111111);

    CHECK(getOffsetBits<1, 3>(0b11111111) == 0b00000001);
    CHECK(getOffsetBits<2, 3>(0b11111111) == 0b00000011);
    CHECK(getOffsetBits<3, 3>(0b11111111) == 0b00000111);
    CHECK(getOffsetBits<4, 3>(0b11111111) == 0b00001111);
    CHECK(getOffsetBits<5, 3>(0b11111111) == 0b00011111);
    CHECK(getOffsetBits<6, 3>(0b11111111) == 0b00011111);
    CHECK(getOffsetBits<7, 3>(0b11111111) == 0b00011111);
    CHECK(getOffsetBits<8, 3>(0b11111111) == 0b00011111);

    CHECK(getOffsetBits<1, 7>(0b11111111) == 0b00000001);
    CHECK(getOffsetBits<2, 7>(0b11111111) == 0b00000001);
    CHECK(getOffsetBits<3, 7>(0b11111111) == 0b00000001);
    CHECK(getOffsetBits<4, 7>(0b11111111) == 0b00000001);
    CHECK(getOffsetBits<5, 7>(0b11111111) == 0b00000001);
    CHECK(getOffsetBits<6, 7>(0b11111111) == 0b00000001);
    CHECK(getOffsetBits<7, 7>(0b11111111) == 0b00000001);
    CHECK(getOffsetBits<8, 7>(0b11111111) == 0b00000001);

    CHECK(getOffsetBits<1, 0>(0b10011001) == 0b00000001);
    CHECK(getOffsetBits<2, 0>(0b10011001) == 0b00000001);
    CHECK(getOffsetBits<3, 0>(0b10011001) == 0b00000001);
    CHECK(getOffsetBits<4, 0>(0b10011001) == 0b00001001);
    CHECK(getOffsetBits<5, 0>(0b10011001) == 0b00011001);
    CHECK(getOffsetBits<6, 0>(0b10011001) == 0b00011001);
    CHECK(getOffsetBits<7, 0>(0b10011001) == 0b00011001);
    CHECK(getOffsetBits<8, 0>(0b10011001) == 0b10011001);

    CHECK(getOffsetBits<1, 2>(0b10011001) == 0b00000000);
    CHECK(getOffsetBits<2, 2>(0b10011001) == 0b00000010);
    CHECK(getOffsetBits<3, 2>(0b10011001) == 0b00000110);
    CHECK(getOffsetBits<4, 2>(0b10011001) == 0b00000110);
    CHECK(getOffsetBits<5, 2>(0b10011001) == 0b00000110);
    CHECK(getOffsetBits<6, 2>(0b10011001) == 0b00100110);
    CHECK(getOffsetBits<7, 2>(0b10011001) == 0b00100110);
    CHECK(getOffsetBits<8, 2>(0b10011001) == 0b00100110);

    CHECK(getOffsetBits<1, 6>(0b10011001) == 0b00000000);
    CHECK(getOffsetBits<2, 6>(0b10011001) == 0b00000010);
    CHECK(getOffsetBits<3, 6>(0b10011001) == 0b00000010);
    CHECK(getOffsetBits<4, 6>(0b10011001) == 0b00000010);
    CHECK(getOffsetBits<5, 6>(0b10011001) == 0b00000010);
    CHECK(getOffsetBits<6, 6>(0b10011001) == 0b00000010);
    CHECK(getOffsetBits<7, 6>(0b10011001) == 0b00000010);
    CHECK(getOffsetBits<8, 6>(0b10011001) == 0b00000010);
}
