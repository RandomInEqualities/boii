#include "gb_bit_utilities.hpp"
#include "gb_exception.hpp"

#include <catch2/catch_test_macros.hpp>

/**
 * Tests for the assert macro.
 */

TEST_CASE("assert")
{
    CHECK_THROWS_AS(
        []
        {
            GB_ASSERT(false);
        }(),
        gb::AssertException);

    CHECK_NOTHROW(
        []
        {
            GB_ASSERT(true);
        }());
}
