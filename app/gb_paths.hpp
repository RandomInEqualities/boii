#pragma once

#include <filesystem>

namespace gb::ui
{

/**
 * Paths to a Game Boy game and for where to put debug logging files.
 */
struct GamePaths
{
    /**
     * The path to the ROM of the Game Boy game.
     */
    std::filesystem::path romFile;

    /**
     * The path to the RAM of the Game Boy game (might not exist).
     */
    std::filesystem::path ramFile;

    /**
     * File to log debug information in (e.g. registers and instructions).
     */
    std::filesystem::path logFile;

    /**
     * Folder to put debug Game Boy screen images in.
     */
    std::filesystem::path debugScreenFolder;

    /**
     * Folder to put debug images of the Game Boy's tile map in.
     */
    std::filesystem::path debugTileFolder;
};

} // namespace gb::ui
