#include "gb_byte.hpp"
#include "gb_cartridge.hpp"
#include "gb_exception.hpp"
#include "gb_logger.hpp"
#include "gb_paths.hpp"
#include "gb_processor.hpp"
#include "gb_window.hpp"

#include <cxxopts.hpp>

#include <cstdlib>
#include <exception>
#include <filesystem>
#include <iostream>
#include <string>

static gb::ByteVector readRom(const std::filesystem::path& romPath)
{
    if (is_regular_file(romPath))
    {
        return gb::readBytesFromFile(romPath);
    }
    else
    {
        throw gb::IoException("Specified ROM path does not point to a valid file: " + romPath.string());
    }
}

static void loadRam(gb::Cartridge& cartridge, const std::filesystem::path& ramPath)
{
    if (is_regular_file(ramPath))
    {
        cartridge.setRam(gb::readBytesFromFile(ramPath));
    }
}

static void saveRam(const gb::Cartridge& cartridge, const std::filesystem::path& ramPath)
{
    const gb::ByteVector ram = cartridge.ram();
    if (!ram.empty())
    {
        gb::writeBytesToFile(ramPath, ram);
    }
}

static gb::ui::GamePaths readCommandLine(const int argc, const char* const* argv)
{
    /**
     * Setup command line options and help text.
     */
    cxxopts::Options options("Gameboi", "Emulator for the Nintendo Game Boy.");
    options.add_options()("p,path", "Path to the ROM of the Game Boy game.", cxxopts::value<std::string>());
    options.add_options()("h,help", "Show help information.");
    options.parse_positional({"path"});
    options.positional_help("path");
    options.show_positional_help();

    /**
     * Parse commandline and check that required options are set.
     */
    const cxxopts::ParseResult commandLine = options.parse(argc, argv);
    if (commandLine["help"].count() > 0)
    {
        std::cout << options.help() << std::endl;
        std::exit(EXIT_SUCCESS);
    }
    if (commandLine["path"].count() == 0)
    {
        std::cerr << options.help() << std::endl;
        std::exit(EXIT_FAILURE);
    }

    /**
     * Read the game paths from the parsed command line.
     */
    gb::ui::GamePaths paths;
    paths.romFile = commandLine["path"].as<std::string>();
    paths.ramFile = std::filesystem::path(paths.romFile).replace_extension("sav");
    paths.logFile = std::filesystem::path(paths.romFile).replace_filename("log.txt");
    paths.debugScreenFolder = std::filesystem::path(paths.romFile).replace_filename("log_screen");
    paths.debugTileFolder = std::filesystem::path(paths.romFile).replace_filename("log_tile_map");
    return paths;
}

int main(const int argc, char** argv)
{
    try
    {
        const gb::ui::GamePaths gamePaths = readCommandLine(argc, argv);

        /**
         * Load the ROM image of the game that we will be playing into a cartridge.
         */
        gb::Cartridge cartridge(readRom(gamePaths.romFile));

        /**
         * Load any saved RAM for the game if it has saved game state.
         */
        loadRam(cartridge, gamePaths.ramFile);

        /**
         * Plug the cartridge into the processor.
         */
        gb::Processor processor(cartridge);

        /**
         * Create window that we will display the game in.
         */
        gb::ui::Window window(processor.input(), processor.lcd(), gamePaths);

        /**
         * Run the processor loop.
         */
        while (window.isOpen())
        {
            /**
             * For every loop iteration we execute 1 instruction.
             */
            processor.step(window.getLogger());

            /**
             * Check for user input.
             */
            window.processEvents();
        }

        /**
         * Save the RAM so the game can be restored to same state.
         */
        saveRam(cartridge, gamePaths.ramFile);
        return EXIT_SUCCESS;
    }
    catch (const cxxopts::exceptions::exception& exception)
    {
        std::cerr << "Error: " << exception.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const gb::Exception& exception)
    {
        std::cerr << "GB Error: " << exception.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const std::ios::failure& exception)
    {
        std::cerr << "IO Error: " << exception.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const std::exception& exception)
    {
        std::cerr << "STD Error: " << exception.what() << std::endl;
        return EXIT_FAILURE;
    }
}
