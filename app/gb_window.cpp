#include "gb_window.hpp"
#include "gb_exception.hpp"
#include "gb_int_cast.hpp"

#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Image.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/VideoMode.hpp>

#include <filesystem>
#include <optional>
#include <string>
#include <thread>

namespace gb::ui
{

Window::Window(InputSystem& input, const LcdSystem& lcd, GamePaths gamePaths)
    : m_input(input), m_lcd(lcd), m_paths(std::move(gamePaths))
{
    const uint32 screenWidth = int_cast<uint32>(m_lcd.getScreenImage().width());
    const uint32 screenHeight = int_cast<uint32>(m_lcd.getScreenImage().height());
    constexpr uint32 renderMargins = 10;
    const uint32 renderWidth = screenWidth + 2 * renderMargins;
    const uint32 renderHeight = screenHeight + 2 * renderMargins;
    const sf::VideoMode mode(renderWidth, renderHeight);
    m_window.create(mode, "Gameboi");

    constexpr uint32 scalingFactor = 4;
    const sf::Vector2u windowSize{scalingFactor * renderWidth, scalingFactor * renderHeight};
    m_window.setSize(windowSize);

    const sf::Vector2i windowPosition{300, 100};
    m_window.setPosition(windowPosition);

    m_texture.create(screenWidth, screenHeight);
    m_sprite.setTexture(m_texture, true);
    m_sprite.setPosition(renderMargins, renderMargins);

    m_window.setFramerateLimit(m_fpsLimit);
    m_window.setKeyRepeatEnabled(false);

    setDebugModeEnabled(m_inDebugMode);
    setVerticalSyncEnabled(m_verticalSyncEnabled);
    setIsPaused(m_isPaused);
    refreshWindow();
}

Window::~Window()
{
    /**
     * Clear log to not crash if a stream in the logs destructor throws when stack is unwinding.
     */
    m_log = Logger();
}

bool Window::isOpen() const
{
    return m_window.isOpen();
}

void Window::processEvents()
{
    /**
     * When the emulator has a vblank interrupt it has finished rendering a frame.
     */
    if (m_lcd.haveVBlankInterrupt())
    {
        m_vblankCount++;
        refreshWindow();
        dumpImages();
    }

    /**
     * Limit polling for input events, it is a bit slow and process events is called a lot.
     */
    if (isTimeForPoll())
    {
        pollEvents();
        waitForUnpause();
    }
}

Logger& Window::getLogger()
{
    return m_log;
}

void Window::refreshWindow()
{
    /**
     * Get the pixels in the Game Boy screen image.
     */
    const LcdImage image = m_lcd.getScreenImage();
    const GLsizei width = int_cast<GLsizei>(image.width());
    const GLsizei height = int_cast<GLsizei>(image.height());
    const std::vector<LcdColor>& pixels = image.pixels();

    /**
     * Put the emulator image into the OpenGL texture (transforms from RGB to RGBA via
     * glTexImage2D). This way gives a speed improvement when the emulator is compiled
     * in Debug mode.
     */
    static_assert(sizeof(LcdColor) == 3, "tight packing is required");
    sf::Texture::bind(&m_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, pixels.data());

    /**
     * Draw the new texture with SFML.
     */
    m_window.resetGLStates();
    m_window.clear(sf::Color::Black);
    m_window.draw(m_sprite);
    m_window.display();
}

static void saveLcdImage(const LcdImage& lcdImage, const std::filesystem::path& savePath)
{
    const uint32 width = int_cast<uint32>(lcdImage.width());
    const uint32 height = int_cast<uint32>(lcdImage.height());
    const std::vector<uint8> rgba = lcdImage.toRgba();
    sf::Image image;
    image.create(width, height, rgba.data());
    if (!image.saveToFile(savePath.string()))
    {
        throw IoException("could not save image to: " + savePath.string());
    }
}

void Window::dumpImages()
{
    if (m_inDebugMode)
    {
        /**
         * Dump the main screen image to screen folder.
         */
        const std::string screenFilename = "screen_" + std::to_string(m_vblankCount) + ".png";
        saveLcdImage(m_lcd.getScreenImage(), m_paths.debugScreenFolder / screenFilename);

        /**
         * Dump the image showing the tile memory to tile folder.
         */
        const std::string tileFilename = "tile_" + std::to_string(m_vblankCount) + ".png";
        saveLcdImage(m_lcd.getTileImage(), m_paths.debugTileFolder / tileFilename);

        /**
         * Mark in the log for where images were dumped.
         */
        m_log() << "VBLANK" << m_vblankCount;
    }
}

bool Window::isTimeForPoll()
{
    /**
     * Wait 50 instructions before checking the polling clock, it can be slow to check.
     */
    if (m_pollInstructionCount < 50)
    {
        m_pollInstructionCount++;
        return false;
    }
    else
    {
        m_pollInstructionCount = 0;
    }

    /**
     * Check if enough time has elapsed between checking for player input.
     */
    if (m_pollClock.getElapsedTime().asMicroseconds() > 500)
    {
        m_pollClock.restart();
        return true;
    }
    else
    {
        return false;
    }
}

void Window::pollEvents()
{
    sf::Event event{};
    while (m_window.pollEvent(event))
    {
        if (!consumeGameInput(event))
        {
            consumeWindowInput(event);
        }
    }
}

void Window::waitForUnpause()
{
    while (m_isPaused)
    {
        constexpr std::chrono::milliseconds sleepTime{16};
        std::this_thread::sleep_for(sleepTime);
        pollEvents();
    }
}

static std::optional<Key> getGameBoyKey(const sf::Keyboard::Key sfmlKey)
{
    switch (sfmlKey)
    {
    case sf::Keyboard::Key::Up:
        return Key::Up;
    case sf::Keyboard::Key::Down:
        return Key::Down;
    case sf::Keyboard::Key::Left:
        return Key::Left;
    case sf::Keyboard::Key::Right:
        return Key::Right;
    case sf::Keyboard::Key::X:
        return Key::A;
    case sf::Keyboard::Key::Z:
        return Key::B;
    case sf::Keyboard::Key::A:
        return Key::Start;
    case sf::Keyboard::Key::S:
        return Key::Select;
    case sf::Keyboard::Key::Escape:
        return Key::Start;
    case sf::Keyboard::Key::BackSpace:
        return Key::Select;
    default:
        return std::nullopt;
    }
}

bool Window::consumeGameInput(const sf::Event& event)
{
    const bool isKeyPress = (event.type == sf::Event::KeyPressed);
    const bool isKeyRelease = (event.type == sf::Event::KeyReleased);
    if (!isKeyPress && !isKeyRelease)
    {
        return false;
    }

    const std::optional<Key> gameKey = getGameBoyKey(event.key.code);
    if (!gameKey)
    {
        return false;
    }

    m_input.setPressed(*gameKey, isKeyPress);
    return true;
}

bool Window::consumeWindowInput(const sf::Event& event)
{
    if (event.type == sf::Event::Closed)
    {
        m_window.close();
        return true;
    }
    if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Key::R)
    {
        setDebugModeEnabled(!m_inDebugMode);
        return true;
    }
    if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Key::V)
    {
        setVerticalSyncEnabled(!m_verticalSyncEnabled);
        return true;
    }
    if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Key::P)
    {
        setIsPaused(!m_isPaused);
        return true;
    }
    return false;
}

void Window::setDebugModeEnabled(const bool isEnabled)
{
    m_inDebugMode = isEnabled;
    m_log.flush();

    if (isEnabled)
    {
        initializeLogFile();
        m_log = Logger({m_logFile});
    }
    else
    {
        m_log = Logger();
    }
}

void Window::initializeLogFile()
{
    if (m_logFile.is_open())
    {
        return;
    }

    const auto createLogFile = [](const std::filesystem::path& logFilePath)
    {
        std::ofstream logFile;
        logFile.exceptions(std::ofstream::failbit | std::ofstream::badbit | std::ofstream::eofbit);
        logFile.open(logFilePath, std::ios::out | std::ios::trunc);
        return logFile;
    };
    const auto clearLogFolder = [](const std::filesystem::path& folder)
    {
        remove_all(folder);
        create_directory(folder);
    };

    m_logFile = createLogFile(m_paths.logFile);

    /**
     * Remove existing images in debug folders.
     */
    clearLogFolder(m_paths.debugScreenFolder);
    clearLogFolder(m_paths.debugTileFolder);
}

void Window::setVerticalSyncEnabled(const bool isEnabled)
{
    m_verticalSyncEnabled = isEnabled;
    m_window.setVerticalSyncEnabled(isEnabled);
}

void Window::setIsPaused(const bool isPaused)
{
    m_isPaused = isPaused;
}

} // namespace gb::ui
