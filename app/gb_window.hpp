#pragma once

#include "gb_input_system.hpp"
#include "gb_int.hpp"
#include "gb_lcd_system.hpp"
#include "gb_logger.hpp"
#include "gb_paths.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>

#include <fstream>

namespace gb::ui
{

/**
 * SFML window to display Game Boy graphics and handle user inputs.
 */
class Window
{
public:
    Window(InputSystem&, const LcdSystem&, GamePaths);
    ~Window();

    /**
     * Check if the window is open and active.
     */
    bool isOpen() const;

    /**
     * Process any user input events.
     */
    void processEvents();

    /**
     * Get the logger that should be used to log debug information.
     */
    Logger& getLogger();

private:
    void refreshWindow();
    void dumpImages();

    bool isTimeForPoll();
    void pollEvents();
    void waitForUnpause();
    bool consumeGameInput(const sf::Event& event);
    bool consumeWindowInput(const sf::Event& event);

    void setDebugModeEnabled(bool isEnabled);
    void initializeLogFile();
    void setVerticalSyncEnabled(bool isEnabled);
    void setIsPaused(bool isPaused);

    InputSystem& m_input;
    const LcdSystem& m_lcd;
    const GamePaths m_paths;

    sf::RenderWindow m_window;
    sf::Texture m_texture;
    sf::Sprite m_sprite;
    sf::Clock m_pollClock;
    int32 m_pollInstructionCount = 0;

    std::ofstream m_logFile;
    Logger m_log;

    const uint32 m_fpsLimit = 240;
    bool m_inDebugMode = false;
    bool m_verticalSyncEnabled = false;
    bool m_isPaused = false;
    int64 m_vblankCount = 0;
};

} // namespace gb::ui
